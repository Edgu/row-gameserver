<?xml version="1.0" encoding="UTF-8"?>
<tileset name="fish" tilewidth="32" tileheight="32" tilecount="1" columns="1">
 <image source="fish.png" width="32" height="32"/>
 <tile id="0">
  <properties>
   <property name="name" value="fish"/>
   <property name="resource" type="bool" value="false"/>
   <property name="solid" type="bool" value="false"/>
  </properties>
 </tile>
</tileset>
