<?xml version="1.0" encoding="UTF-8"?>
<tileset name="fish_depleted" tilewidth="32" tileheight="32" tilecount="1" columns="1">
 <properties>
  <property name="name" value="fish_depleted"/>
  <property name="solid" type="bool" value="false"/>
 </properties>
 <image source="fish_depleted.png" width="32" height="32"/>
 <tile id="0">
  <properties>
   <property name="name" value="fish_depleted"/>
   <property name="solid" type="bool" value="false"/>
  </properties>
 </tile>
</tileset>
