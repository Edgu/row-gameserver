package com.realmofwrath.game.gameserver;

import java.util.Scanner;

public class CommandListener implements Runnable {

	private String command;
	private boolean newCommand = false;
	private Logger logger;

	public CommandListener(Logger logger) {
		this.logger = logger;
	}

	@Override
	public void run() {
		Scanner in = new Scanner(System.in);

		logger.output("[Launch] <CommandListener succesfully started>");

		while (true) {
			command = in.nextLine();
			newCommand = true;

			if (newCommand){
				newCommand = false;
				switch (command) {
				case "":
				case " ":
					break;
				case "help":
					logger.output("[CommandListener] <Help command executed>");
					System.out.println("Available commands are: \n"
							+ "help, restart, shutdown");
					break;
				case "restart":
					logger.output("[CommandListener] <Restart command executed>");
					break;
				case "shutdown":
					logger.output("[CommandListener] <Shutdown command executed>");
					in.close();
					System.exit(0);
					break;
				default:
					logger.output("[CommandListener] <Invalid Command: " + command + ">");
					break;
				}
			}
		}
	}
}