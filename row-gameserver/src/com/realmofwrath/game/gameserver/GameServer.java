package com.realmofwrath.game.gameserver;

import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.mockito.Mockito;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.headless.HeadlessApplication;
import com.badlogic.gdx.backends.headless.HeadlessApplicationConfiguration;
import com.badlogic.gdx.graphics.GL20;
import com.realmofwrath.game.gameserver.managers.EntityManager;
import com.realmofwrath.game.gameserver.network.World;
import com.realmofwrath.game.gameserver.sql.Database;

public class GameServer implements ApplicationListener {

	public static final int PORT = 27015;
	public static final boolean LOG = false;

	private HeadlessApplicationConfiguration config;

	private EntityManager entityManager;

	private Logger logger;
	private SimpleDateFormat timeStamp;
	private Calendar date;
	private CommandListener commandListener;

	private final static int TICKS_PER_SECOND = 60;
	private final float STEP = 1f / TICKS_PER_SECOND;
	private float accumulator;

	// Game Systems Constants
	public static final int TREE_GROWTH_INTERVAL = TICKS_PER_SECOND;
	public static final int WOODCUTTING_INTERVAL = TICKS_PER_SECOND;
	public static final int ROCK_REPLENISH_INTERVAL = TICKS_PER_SECOND;
	public static final int MINING_INTERVAL = TICKS_PER_SECOND;
	public static final int FISH_REPLENISH_INTERVAL = TICKS_PER_SECOND;
	public static final int FISHING_INTERVAL = TICKS_PER_SECOND;
	public static final int RANDOM_MOVE_INTERVAL = TICKS_PER_SECOND * 5;
	public static final int SPAWN_SYSTEM_INTERVAL = TICKS_PER_SECOND;
	public static final int LOOT_SYSTEM_INTERVAL = TICKS_PER_SECOND;

	// MISC constants
	public static final int DROP_PUBLIC_OWNERSHIP = 10; // Seconds
	public static final int DROP_REMOVE = 60; // Seconds

	public static void main(String[] args) {
		new GameServer();
	}

	public GameServer() {
		date = Calendar.getInstance();
		timeStamp = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		date.getTime();

		try {
			logger = new Logger("GameServer-" + timeStamp.format(date.getTime()) + ".txt");
		} catch (IOException e) {
			e.printStackTrace();
		}

		Database.init(logger);
		commandListener = new CommandListener(logger);
		new Thread(commandListener).start();

		config = new HeadlessApplicationConfiguration();
		new HeadlessApplication(this, config);

		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
			public void run() {
				try {
					Database.terminate();
				} catch (SQLException exception) {
					exception.printStackTrace();
				}
			}
		}));
	}

	@Override
	public void create() {
		Gdx.gl = Mockito.mock(GL20.class);
		entityManager = EntityManager.getInstance();

		// INIT
		World.getInstance();

		//World.getInstance().createDrop(null, 0, 3, 4);
	}

	@Override
	public void render() {
		accumulator += Gdx.graphics.getDeltaTime();

		while (accumulator >= STEP) {
			entityManager.update(1);
			accumulator -= STEP;
		}
	}

	@Override
	public void resize(int width, int height) {

	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void dispose() {

	}

}