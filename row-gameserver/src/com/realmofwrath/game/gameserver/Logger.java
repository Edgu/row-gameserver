package com.realmofwrath.game.gameserver;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Logger{

	private File f;
	private BufferedWriter out;
	private FileWriter fw;
	private Calendar date;
	private SimpleDateFormat timeStamp;

	public Logger(String fileLoc) throws IOException {
		if (GameServer.LOG) {
			f = new File(fileLoc);
			//		if (f.getParentFile().exists()) {
			//			f.createNewFile();
			//		} else {
			//			f.getParentFile().mkdir();
			//			f.createNewFile();
			//		}
			try {
				fw = new FileWriter(f, true);
				out = new BufferedWriter(fw);
			} catch (IOException e) {
				System.out.println("ERROR: Log initializing.");
			}
		}
	}

	public void output(String text) {
		date = Calendar.getInstance();
		timeStamp = new SimpleDateFormat("[yyyy/MM/dd HH:mm:ss]");
		date.getTime();
		System.out.println(timeStamp.format(date.getTime()) + " " + text);
		if (GameServer.LOG) {
			try {
				out.write(timeStamp.format(date.getTime()) + " " + text + System.getProperty("line.separator"));
				out.flush();
			} catch (IOException e) {
				System.out.println("ERROR: Log writing.");
			}
		}
	}
}