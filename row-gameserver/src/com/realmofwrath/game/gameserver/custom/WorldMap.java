package com.realmofwrath.game.gameserver.custom;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.utils.Array;
import com.realmofwrath.game.custom.Map;
import com.realmofwrath.game.gameserver.ecs.Mappers;
import com.realmofwrath.game.network.packet.PacketNPCKill;
import com.realmofwrath.game.network.packet.player.PacketRemovePlayer;

public class WorldMap extends Map {

	private Array<Entity> players;
	private Array<Entity> npcs;
	private ImmutableArray<Entity> rocks;
	private ImmutableArray<Entity> trees;
	private ImmutableArray<Entity> fishes;

	public WorldMap(int id, TiledMap tiledMap, ImmutableArray<Entity> rocks, ImmutableArray<Entity> trees, ImmutableArray<Entity> fishes) {
		super(id, tiledMap);
		players = new Array<Entity>(0);
		this.rocks = rocks;
		this.trees = trees;
		this.fishes = fishes;
		npcs = new Array<Entity>(0);
	}

	// Disallows to add or delete from the list.
	public ImmutableArray<Entity> getPlayers() {
		return new ImmutableArray<Entity>(players);
	}

	public ImmutableArray<Entity> getNPCs() {
		return new ImmutableArray<Entity>(npcs);
	}

	public ImmutableArray<Entity> getRocks() {
		return rocks;
	}

	public ImmutableArray<Entity> getTrees() {
		return trees;
	}

	public ImmutableArray<Entity> getFishes() {
		return fishes;
	}

	public void addPlayer(Entity entity) {
		players.add(entity);
	}

	public void addNPC(Entity entity) {
		npcs.add(entity);
	}

	public void removeNPC(Entity npc) {
		npcs.removeValue(npc, true);

		PacketNPCKill packet = new PacketNPCKill();
		packet.instanceId = Mappers.NPC_CM.get(npc).instanceId;

		sendToAll(packet);
	}

	public void removePlayer(Entity player) {
		players.removeValue(player, true);

		PacketRemovePlayer packet = new PacketRemovePlayer();
		packet.id = Mappers.ID_CM.get(player).id;

		sendToAll(packet);
	}

	public void sendToAll(Object packet) {
		for (Entity player : players) {
			Mappers.CON_CM.get(player).connection.sendTCP(packet);
		}
	}

}