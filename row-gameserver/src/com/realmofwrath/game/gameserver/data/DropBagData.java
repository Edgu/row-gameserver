package com.realmofwrath.game.gameserver.data;

public class DropBagData {

	public final int NPC_ID;
	public final int ITEM_ID;
	public final int RARITY_ID;
	public final int CHANCE;
	
	public DropBagData(int npcId, int itemId, int rarityId, int chance) {
		NPC_ID = npcId;
		ITEM_ID = itemId;
		RARITY_ID = rarityId;
		CHANCE = chance;
	}
	
}