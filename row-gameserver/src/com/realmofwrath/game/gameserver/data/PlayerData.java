package com.realmofwrath.game.gameserver.data;

import com.realmofwrath.game.custom.Vector2Integer;

public class PlayerData {

	private int id;
	private String name;
	private int mapId;
	private Vector2Integer position;

	public PlayerData(int id, String name, int mapId, int x, int y) {
		this.id = id;
		this.name = name;
		this.mapId = mapId;
		position = new Vector2Integer(x, y);
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public int getMapId() {
		return mapId;
	}

	public Vector2Integer getPosition() {
		return position;
	}

}