package com.realmofwrath.game.gameserver.ecs;

import com.badlogic.ashley.core.ComponentMapper;
import com.realmofwrath.game.gameserver.ecs.components.AttackComponent;
import com.realmofwrath.game.gameserver.ecs.components.ConnectionComponent;
import com.realmofwrath.game.gameserver.ecs.components.DropBagComponent;
import com.realmofwrath.game.gameserver.ecs.components.EntityTypeComponent;
import com.realmofwrath.game.gameserver.ecs.components.FollowComponent;
import com.realmofwrath.game.gameserver.ecs.components.HealthComponent;
import com.realmofwrath.game.gameserver.ecs.components.IdComponent;
import com.realmofwrath.game.gameserver.ecs.components.InventoryComponent;
import com.realmofwrath.game.gameserver.ecs.components.MapComponent;
import com.realmofwrath.game.gameserver.ecs.components.MovementQueueComponent;
import com.realmofwrath.game.gameserver.ecs.components.NPCComponent;
import com.realmofwrath.game.gameserver.ecs.components.PositionComponent;
import com.realmofwrath.game.gameserver.ecs.components.RandomWalkComponent;
import com.realmofwrath.game.gameserver.ecs.components.SpawnComponent;
import com.realmofwrath.game.gameserver.ecs.components.StateComponent;
import com.realmofwrath.game.gameserver.ecs.components.VelocityComponent;
import com.realmofwrath.game.gameserver.ecs.components.items.ItemComponent;
import com.realmofwrath.game.gameserver.ecs.components.skills.ReplenishComponent;
import com.realmofwrath.game.gameserver.ecs.components.skills.SkillComponent;
import com.realmofwrath.game.gameserver.ecs.components.skills.fishing.FishableComponent;
import com.realmofwrath.game.gameserver.ecs.components.skills.fishing.FishingComponent;
import com.realmofwrath.game.gameserver.ecs.components.skills.mining.MinableComponent;
import com.realmofwrath.game.gameserver.ecs.components.skills.mining.MiningComponent;
import com.realmofwrath.game.gameserver.ecs.components.skills.woodcutting.ChoppableComponent;
import com.realmofwrath.game.gameserver.ecs.components.skills.woodcutting.WoodcuttingComponent;

public class Mappers {

	public static final ComponentMapper<PositionComponent> POS_CM = ComponentMapper.getFor(PositionComponent.class);
	public static final ComponentMapper<VelocityComponent> VEL_CM = ComponentMapper.getFor(VelocityComponent.class);
	public static final ComponentMapper<MovementQueueComponent> MQU_CM = ComponentMapper.getFor(MovementQueueComponent.class);
	public static final ComponentMapper<StateComponent> STA_CM = ComponentMapper.getFor(StateComponent.class);
	public static final ComponentMapper<ReplenishComponent> REP_CM = ComponentMapper.getFor(ReplenishComponent.class);
	public static final ComponentMapper<IdComponent> ID_CM = ComponentMapper.getFor(IdComponent.class);
	public static final ComponentMapper<ConnectionComponent> CON_CM = ComponentMapper.getFor(ConnectionComponent.class);
	public static final ComponentMapper<NPCComponent> NPC_CM = ComponentMapper.getFor(NPCComponent.class);
	public static final ComponentMapper<AttackComponent> ATT_CM = ComponentMapper.getFor(AttackComponent.class);
	public static final ComponentMapper<HealthComponent> HP_CM = ComponentMapper.getFor(HealthComponent.class);
	public static final ComponentMapper<EntityTypeComponent> TYP_CM = ComponentMapper.getFor(EntityTypeComponent.class);
	public static final ComponentMapper<RandomWalkComponent> RWA_CM = ComponentMapper.getFor(RandomWalkComponent.class);
	public static final ComponentMapper<SpawnComponent> SPAWN_CM = ComponentMapper.getFor(SpawnComponent.class);
	public static final ComponentMapper<InventoryComponent> INV_CM = ComponentMapper.getFor(InventoryComponent.class);
	public static final ComponentMapper<DropBagComponent> DB_CM = ComponentMapper.getFor(DropBagComponent.class);
	public static final ComponentMapper<FollowComponent> FOL_CM = ComponentMapper.getFor(FollowComponent.class);
	public static final ComponentMapper<ItemComponent> ITE_CM = ComponentMapper.getFor(ItemComponent.class);

	// Skills gen
	public static final ComponentMapper<SkillComponent> SKI_CM = ComponentMapper.getFor(SkillComponent.class);

	// Skills
	public static final ComponentMapper<WoodcuttingComponent> WOO_CM = ComponentMapper.getFor(WoodcuttingComponent.class);
	public static final ComponentMapper<MiningComponent> MIN_CM = ComponentMapper.getFor(MiningComponent.class);
	public static final ComponentMapper<FishingComponent> FIS_CM = ComponentMapper.getFor(FishingComponent.class);

	// Skill res
	public static final ComponentMapper<ChoppableComponent> CHO_CM = ComponentMapper.getFor(ChoppableComponent.class);
	public static final ComponentMapper<MinableComponent> MAB_CM = ComponentMapper.getFor(MinableComponent.class);
	public static final ComponentMapper<FishableComponent> FAB_CM = ComponentMapper.getFor(FishableComponent.class);

	// Test
	public static final ComponentMapper<MapComponent> MAP_CM = ComponentMapper.getFor(MapComponent.class);

}