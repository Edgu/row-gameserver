package com.realmofwrath.game.gameserver.ecs.components;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Entity;

public class AttackComponent implements Component {

	public Entity victim;
	public List<Entity> attackers;

	public AttackComponent() {
		attackers = new ArrayList<Entity>();
	}

}