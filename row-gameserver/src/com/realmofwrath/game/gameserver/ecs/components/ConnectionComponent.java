package com.realmofwrath.game.gameserver.ecs.components;

import com.badlogic.ashley.core.Component;
import com.esotericsoftware.kryonet.Connection;

public class ConnectionComponent implements Component {
	
	public Connection connection;
	
	public ConnectionComponent(Connection connection) {
		this.connection = connection;
	}

}