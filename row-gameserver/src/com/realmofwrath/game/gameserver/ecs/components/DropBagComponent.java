package com.realmofwrath.game.gameserver.ecs.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Entity;

public class DropBagComponent implements Component {

	public SlotComponent[] slots;
	public Entity owner;
	public int counter;

	public DropBagComponent(Entity owner, SlotComponent[] slots) {
		this.owner = owner;
		this.slots = slots;
	}

}