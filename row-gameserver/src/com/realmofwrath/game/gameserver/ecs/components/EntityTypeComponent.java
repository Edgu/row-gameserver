package com.realmofwrath.game.gameserver.ecs.components;

import com.badlogic.ashley.core.Component;
import com.realmofwrath.game.gameserver.ecs.entities.EntityType;

public class EntityTypeComponent implements Component {
	
	public final EntityType entityType;
	
	public EntityTypeComponent(EntityType entityType) {
		this.entityType = entityType;
	}
	
}