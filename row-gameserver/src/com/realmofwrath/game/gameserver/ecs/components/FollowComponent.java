package com.realmofwrath.game.gameserver.ecs.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Entity;
import com.realmofwrath.game.custom.Vector2Integer;

public class FollowComponent implements Component {

	public Entity subject;
	public int range;
	public Vector2Integer lastKnownSubjectPosition;
	public boolean recalculate;

	public FollowComponent(Entity subject) {
		this.subject = subject;
		range = 1;
	}

	public FollowComponent(Entity subject, int range) {
		this.subject = subject;
		this.range = range;
	}

}