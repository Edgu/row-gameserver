package com.realmofwrath.game.gameserver.ecs.components;

import com.badlogic.ashley.core.Component;

public class HealthComponent implements Component {

	public final int maxHealth;
	public int health;
	
	public HealthComponent(int health, int maxHealth) {
		this.health = health;
		this.maxHealth = maxHealth;
	}
	
	public HealthComponent(int health) {
		this(health, health);
	}
	
}