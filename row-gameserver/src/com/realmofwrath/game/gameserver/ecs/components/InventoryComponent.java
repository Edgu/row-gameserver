package com.realmofwrath.game.gameserver.ecs.components;

import com.badlogic.ashley.core.Component;

public class InventoryComponent implements Component {

	public SlotComponent[] slots;

	public InventoryComponent(SlotComponent[] slots) {
		this.slots = slots;
	}

}