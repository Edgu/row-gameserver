package com.realmofwrath.game.gameserver.ecs.components;

import com.badlogic.ashley.core.Component;

public class LevelComponent implements Component {

	public int level;
	public int xp;
	
	public LevelComponent(int level, int xp) {
		this.level = level;
		this.xp = xp;
	}
	
	public LevelComponent(int level) {
		this.level = level;
	}
	
}