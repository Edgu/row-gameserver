package com.realmofwrath.game.gameserver.ecs.components;

import com.badlogic.ashley.core.Component;

public class MapComponent implements Component {

	public int mapId;

	public MapComponent(int mapId) {
		this.mapId = mapId;
	}

}