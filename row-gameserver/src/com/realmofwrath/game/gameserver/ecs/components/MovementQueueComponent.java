package com.realmofwrath.game.gameserver.ecs.components;

import java.util.LinkedList;
import java.util.Queue;

import com.badlogic.ashley.core.Component;
import com.realmofwrath.game.custom.Vector2Integer;

public class MovementQueueComponent implements Component {

	public Queue<Vector2Integer> queue;
	public Vector2Integer currentMove;

	public MovementQueueComponent() {
		queue = new LinkedList<Vector2Integer>();
	}

}