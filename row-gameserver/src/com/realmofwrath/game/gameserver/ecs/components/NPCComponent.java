package com.realmofwrath.game.gameserver.ecs.components;

import com.badlogic.ashley.core.Component;

public class NPCComponent implements Component {

	public final int id;
	public final int instanceId;
	
	public NPCComponent(int id, int instanceId) {
		this.id = id;
		this.instanceId = instanceId;
	}
	
}