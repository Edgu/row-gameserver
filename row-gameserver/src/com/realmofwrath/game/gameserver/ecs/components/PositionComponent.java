package com.realmofwrath.game.gameserver.ecs.components;

import com.badlogic.ashley.core.Component;
import com.realmofwrath.game.custom.Vector2Integer;

public class PositionComponent implements Component {

	public Vector2Integer position;

	public PositionComponent(int x, int y) {
		position = new Vector2Integer(x, y);
	}
	
	public PositionComponent(Vector2Integer position) {
		this.position = position;
	}

}