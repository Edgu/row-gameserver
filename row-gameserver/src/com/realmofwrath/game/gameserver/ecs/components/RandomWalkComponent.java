package com.realmofwrath.game.gameserver.ecs.components;

import java.util.List;

import com.badlogic.ashley.core.Component;
import com.realmofwrath.game.custom.Vector2Integer;

public class RandomWalkComponent implements Component {

	public List<Vector2Integer> area;

	public RandomWalkComponent(List<Vector2Integer> area) {
		this.area = area;
	}

}