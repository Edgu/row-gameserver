package com.realmofwrath.game.gameserver.ecs.components;

import com.badlogic.ashley.core.Component;
import com.realmofwrath.game.gameserver.ecs.entities.Item;
import com.realmofwrath.game.inventory.ItemType;

public class SlotComponent implements Component {

	public ItemType type;
	public Item item;
	
	public SlotComponent(ItemType type, Item item) {
		this.type = type;
		this.item = item;
	}
	
	public SlotComponent(ItemType type) {
		this(type, null);
	}
	
	public SlotComponent(Item item) {
		this(ItemType.DEFAULT, item);
	}

}