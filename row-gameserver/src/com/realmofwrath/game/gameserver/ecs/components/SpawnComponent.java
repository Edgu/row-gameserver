package com.realmofwrath.game.gameserver.ecs.components;

import com.badlogic.ashley.core.Component;

public class SpawnComponent implements Component {

	public final int x, y;
	public final int respawnTime;
	public int counter;
	
	public SpawnComponent(int x, int y, int respawnTime) {
		this.x = x;
		this.y = y;
		this.respawnTime = respawnTime;
	}
	
}