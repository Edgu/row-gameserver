package com.realmofwrath.game.gameserver.ecs.components;

import com.badlogic.ashley.core.Component;
import com.realmofwrath.game.gameserver.ecs.entities.EntityState;

public class StateComponent implements Component {

	public EntityState state;
	
	public StateComponent() {
		state = EntityState.IDLE;
	}

}