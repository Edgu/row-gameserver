package com.realmofwrath.game.gameserver.ecs.components;

import com.badlogic.ashley.core.Component;
import com.realmofwrath.game.custom.Vector2Integer;

public class VelocityComponent implements Component {

	public Vector2Integer velocity;
	
	public VelocityComponent() {
		velocity = new Vector2Integer(0, 0);
	}

}