package com.realmofwrath.game.gameserver.ecs.components.items;

import com.badlogic.ashley.core.Component;
import com.realmofwrath.game.inventory.ItemType;

public class ItemComponent implements Component {

	public final ItemType itemType;
	public final AttributeComponent[] attributes;

	public ItemComponent(ItemType itemType, AttributeComponent[] attributes) {
		this.itemType = itemType;
		this.attributes = attributes;
	}

}