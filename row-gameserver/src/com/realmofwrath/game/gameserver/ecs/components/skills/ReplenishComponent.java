package com.realmofwrath.game.gameserver.ecs.components.skills;

import com.badlogic.ashley.core.Component;

public class ReplenishComponent implements Component {
	
	public int seconds;
	
	public ReplenishComponent(int seconds) {
		this.seconds = seconds;
	}
	
}