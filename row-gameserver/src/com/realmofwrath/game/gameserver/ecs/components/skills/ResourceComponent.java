package com.realmofwrath.game.gameserver.ecs.components.skills;

import com.badlogic.ashley.core.Component;

public abstract class ResourceComponent implements Component {

	public int min, max;
	public int resources;

	public ResourceComponent(int min, int max) {
		this.min = min;
		this.max = max;
		resources = (min + max) / 2;
	}
	
}