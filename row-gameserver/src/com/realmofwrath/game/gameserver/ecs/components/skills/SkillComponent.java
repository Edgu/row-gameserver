package com.realmofwrath.game.gameserver.ecs.components.skills;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Entity;

public abstract class SkillComponent implements Component {
	
	public Entity interactee;
	
	public SkillComponent() {
		
	}
	
	public SkillComponent(Entity interactee) {
		this.interactee = interactee;
	}

}