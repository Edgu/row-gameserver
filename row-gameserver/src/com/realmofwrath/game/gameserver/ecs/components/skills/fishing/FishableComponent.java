package com.realmofwrath.game.gameserver.ecs.components.skills.fishing;

import com.realmofwrath.game.gameserver.ecs.components.skills.ResourceComponent;

public class FishableComponent extends ResourceComponent {

	public FishableComponent(int min, int max) {
		super(min, max);
	}

}