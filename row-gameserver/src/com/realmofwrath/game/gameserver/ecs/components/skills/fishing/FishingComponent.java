package com.realmofwrath.game.gameserver.ecs.components.skills.fishing;

import com.badlogic.ashley.core.Entity;
import com.realmofwrath.game.gameserver.ecs.components.skills.SkillComponent;

public class FishingComponent extends SkillComponent {
	
	public FishingComponent() {
		
	}

	public FishingComponent(Entity interactee) {
		super(interactee);
	}

}