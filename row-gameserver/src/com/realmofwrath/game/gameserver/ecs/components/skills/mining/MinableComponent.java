package com.realmofwrath.game.gameserver.ecs.components.skills.mining;

import com.realmofwrath.game.gameserver.ecs.components.skills.ResourceComponent;

public class MinableComponent extends ResourceComponent {

	public MinableComponent(int min, int max) {
		super(min, max);
	}

}