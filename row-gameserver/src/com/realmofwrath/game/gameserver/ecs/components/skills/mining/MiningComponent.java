package com.realmofwrath.game.gameserver.ecs.components.skills.mining;

import com.badlogic.ashley.core.Entity;
import com.realmofwrath.game.gameserver.ecs.components.skills.SkillComponent;

public class MiningComponent extends SkillComponent {
	
	public MiningComponent() {
		
	}

	public MiningComponent(Entity interactee) {
		super(interactee);
	}

}