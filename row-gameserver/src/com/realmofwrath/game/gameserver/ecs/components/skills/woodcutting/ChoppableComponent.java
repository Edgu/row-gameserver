package com.realmofwrath.game.gameserver.ecs.components.skills.woodcutting;

import com.realmofwrath.game.gameserver.ecs.components.skills.ResourceComponent;

public class ChoppableComponent extends ResourceComponent {

	public ChoppableComponent(int min, int max) {
		super(min, max);
	}

}