package com.realmofwrath.game.gameserver.ecs.components.skills.woodcutting;

import com.badlogic.ashley.core.Entity;
import com.realmofwrath.game.gameserver.ecs.components.skills.SkillComponent;

public class WoodcuttingComponent extends SkillComponent {
	
	public WoodcuttingComponent() {
		
	}

	public WoodcuttingComponent(Entity interactee) {
		super(interactee);
	}
	
}