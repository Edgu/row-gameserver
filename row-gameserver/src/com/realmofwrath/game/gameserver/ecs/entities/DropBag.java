package com.realmofwrath.game.gameserver.ecs.entities;

import com.badlogic.ashley.core.Entity;
import com.realmofwrath.game.gameserver.ecs.components.DropBagComponent;
import com.realmofwrath.game.gameserver.ecs.components.IdComponent;
import com.realmofwrath.game.gameserver.ecs.components.PositionComponent;
import com.realmofwrath.game.gameserver.ecs.components.SlotComponent;

public class DropBag extends Entity {

	public DropBag(int id, SlotComponent[] slots, Entity owner, int x, int y) {
		add(new IdComponent(id));
		add(new PositionComponent(x, y));
		add(new DropBagComponent(owner, slots));
	}

}
