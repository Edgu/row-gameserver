package com.realmofwrath.game.gameserver.ecs.entities;

public enum EntityState {
	IDLE,
	MOVING,
	COMBAT,
	WOODCUTTING,
	DEAD,
	MINING,
	FISHING;

	public int getValue() {
		return ordinal();
	}

}