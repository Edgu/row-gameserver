package com.realmofwrath.game.gameserver.ecs.entities;

public enum EntityType {
	MOB(0),
	PLAYER(1);

	private final int typeId;

	private EntityType(int typeId) {
		this.typeId = typeId;
	}

	public int getValue() {
		return typeId;
	}

	public static EntityType getTypeById(int typeId) {
		return (typeId >= 0 && typeId < EntityType.values().length) ? EntityType.values()[typeId] : null;
	}

}