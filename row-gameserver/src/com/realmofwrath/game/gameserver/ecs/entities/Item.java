package com.realmofwrath.game.gameserver.ecs.entities;

import com.badlogic.ashley.core.Entity;
import com.realmofwrath.game.gameserver.ecs.components.IdComponent;
import com.realmofwrath.game.gameserver.ecs.components.NameComponent;
import com.realmofwrath.game.gameserver.ecs.components.items.AttributeComponent;
import com.realmofwrath.game.gameserver.ecs.components.items.ItemComponent;
import com.realmofwrath.game.inventory.ItemType;

public class Item extends Entity {

	public Item(int id, String name, ItemType itemType, AttributeComponent[] attributes) {
		add(new IdComponent(id));
		add(new NameComponent(name));
		add(new ItemComponent(itemType, attributes));
	}

}