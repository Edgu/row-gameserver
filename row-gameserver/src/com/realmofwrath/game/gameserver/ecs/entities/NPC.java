package com.realmofwrath.game.gameserver.ecs.entities;

import java.util.List;

import com.badlogic.ashley.core.Entity;
import com.realmofwrath.game.custom.Vector2Integer;
import com.realmofwrath.game.gameserver.ecs.components.AttackComponent;
import com.realmofwrath.game.gameserver.ecs.components.EntityTypeComponent;
import com.realmofwrath.game.gameserver.ecs.components.HealthComponent;
import com.realmofwrath.game.gameserver.ecs.components.LevelComponent;
import com.realmofwrath.game.gameserver.ecs.components.MapComponent;
import com.realmofwrath.game.gameserver.ecs.components.MovementQueueComponent;
import com.realmofwrath.game.gameserver.ecs.components.NPCComponent;
import com.realmofwrath.game.gameserver.ecs.components.PositionComponent;
import com.realmofwrath.game.gameserver.ecs.components.RandomWalkComponent;
import com.realmofwrath.game.gameserver.ecs.components.SpawnComponent;
import com.realmofwrath.game.gameserver.ecs.components.StateComponent;
import com.realmofwrath.game.gameserver.ecs.components.VelocityComponent;

public class NPC extends Entity {

	public NPC(int instanceId, int id, int mapId, int x, int y, int health, int respawnTime, int attackTime, EntityType entityType, int range, List<Vector2Integer> area) {
		add(new NPCComponent(id, instanceId));
		add(new EntityTypeComponent(entityType));
		add(new PositionComponent(x, y));
		add(new HealthComponent(health));
		add(new VelocityComponent());
		add(new MovementQueueComponent());
		add(new SpawnComponent(x, y, respawnTime));
		add(new StateComponent());
		add(new LevelComponent(1));
		add(new MapComponent(mapId));
		add(new RandomWalkComponent(area));
		add(new AttackComponent());
	}

}
