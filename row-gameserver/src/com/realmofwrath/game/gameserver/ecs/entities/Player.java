package com.realmofwrath.game.gameserver.ecs.entities;

import com.badlogic.ashley.core.Entity;
import com.esotericsoftware.kryonet.Connection;
import com.realmofwrath.game.custom.TiledUtils;
import com.realmofwrath.game.gameserver.data.PlayerData;
import com.realmofwrath.game.gameserver.ecs.components.AttackComponent;
import com.realmofwrath.game.gameserver.ecs.components.ConnectionComponent;
import com.realmofwrath.game.gameserver.ecs.components.EntityTypeComponent;
import com.realmofwrath.game.gameserver.ecs.components.HealthComponent;
import com.realmofwrath.game.gameserver.ecs.components.IdComponent;
import com.realmofwrath.game.gameserver.ecs.components.InventoryComponent;
import com.realmofwrath.game.gameserver.ecs.components.LevelComponent;
import com.realmofwrath.game.gameserver.ecs.components.MapComponent;
import com.realmofwrath.game.gameserver.ecs.components.MovementQueueComponent;
import com.realmofwrath.game.gameserver.ecs.components.PositionComponent;
import com.realmofwrath.game.gameserver.ecs.components.SlotComponent;
import com.realmofwrath.game.gameserver.ecs.components.StateComponent;
import com.realmofwrath.game.gameserver.ecs.components.VelocityComponent;

public class Player extends Entity {

	// FIXME Load player shit from database.
	public Player(Connection connection, PlayerData playerData, SlotComponent[] slots) {
		// FIXME No need id component
		add(new IdComponent(connection.getID()));
		add(new ConnectionComponent(connection));
		add(new EntityTypeComponent(EntityType.PLAYER));
		add(new InventoryComponent(slots));
		add(new HealthComponent(1000, 10000));
		add(new PositionComponent(TiledUtils.positionToMap(playerData.getPosition())));
		add(new VelocityComponent());
		add(new MovementQueueComponent());
		add(new StateComponent());
		add(new LevelComponent(1));
		add(new AttackComponent());
		add(new MapComponent(playerData.getMapId()));
	}

}