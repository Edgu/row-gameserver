package com.realmofwrath.game.gameserver.ecs.entities.data;

import com.realmofwrath.game.gameserver.ecs.entities.EntityType;

public class NPCData {

	public final EntityType TYPE;
	public final String NAME;
	public final int HEALTH;
	public final int DAMAGE;
	public final int RESPAWN_TIME;
	public final int ATTACK_TIME;
	public final int REACH;
	public final int LOOT;
	public final int RADIUS;
	
	public NPCData(int type, String name, int health, int damage, int respawnTime, int attackTime, int reach, int loot, int radius) {
		TYPE = EntityType.getTypeById(type);
		NAME = name;
		HEALTH = health;
		DAMAGE = damage;
		RESPAWN_TIME = respawnTime;
		ATTACK_TIME = attackTime;
		REACH = reach;
		LOOT = loot;
		RADIUS = radius;
	}
	
}
