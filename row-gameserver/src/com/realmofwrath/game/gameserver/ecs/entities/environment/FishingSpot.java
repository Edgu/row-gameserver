package com.realmofwrath.game.gameserver.ecs.entities.environment;

import com.badlogic.ashley.core.Entity;
import com.realmofwrath.game.custom.Vector2Integer;
import com.realmofwrath.game.gameserver.ecs.components.MapComponent;
import com.realmofwrath.game.gameserver.ecs.components.PositionComponent;
import com.realmofwrath.game.gameserver.ecs.components.skills.fishing.FishableComponent;

public class FishingSpot extends Entity {

	public static final int REPLENISH_TIME = 10;

	public FishingSpot(int mapId, int x, int y, int min, int max) {
		add(new MapComponent(mapId));
		add(new PositionComponent(x, y));
		add(new FishableComponent(min, max));
	}

	public FishingSpot(int mapId, Vector2Integer position, int min, int max) {
		this(mapId, position.x, position.y, min, max);
	}

}