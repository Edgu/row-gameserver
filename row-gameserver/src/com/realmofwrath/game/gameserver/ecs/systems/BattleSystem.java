package com.realmofwrath.game.gameserver.ecs.systems;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.realmofwrath.game.custom.Map;
import com.realmofwrath.game.custom.TiledUtils;
import com.realmofwrath.game.custom.Util;
import com.realmofwrath.game.custom.Vector2Integer;
import com.realmofwrath.game.gameserver.custom.WorldMap;
import com.realmofwrath.game.gameserver.ecs.Mappers;
import com.realmofwrath.game.gameserver.ecs.components.AttackComponent;
import com.realmofwrath.game.gameserver.ecs.components.FollowComponent;
import com.realmofwrath.game.gameserver.ecs.components.HealthComponent;
import com.realmofwrath.game.gameserver.ecs.components.MovementQueueComponent;
import com.realmofwrath.game.gameserver.ecs.components.StateComponent;
import com.realmofwrath.game.gameserver.ecs.entities.EntityState;
import com.realmofwrath.game.gameserver.ecs.entities.EntityType;
import com.realmofwrath.game.gameserver.network.World;
import com.realmofwrath.game.network.packet.PacketNPCKill;

public class BattleSystem extends IteratingSystem {

	public BattleSystem() {
		super(Family.all(AttackComponent.class).get());
	}

	@Override
	protected void processEntity(Entity entity, float deltaTime) {
		AttackComponent ac = Mappers.ATT_CM.get(entity);

		if (ac.victim != null) {
			HealthComponent hc = Mappers.HP_CM.get(entity);

			if (hc.health > 0) {

				EntityType victimEntityType = Mappers.TYP_CM.get(ac.victim).entityType;

				Map map = World.getInstance().getMaps().get(Mappers.MAP_CM.get(entity).mapId);

				switch (victimEntityType) {
				case MOB: {// Attacker can only be PLAYER.
					HealthComponent vhc = Mappers.HP_CM.get(ac.victim);

					if (vhc.health > 0) {
						MovementQueueComponent mqc = Mappers.MQU_CM.get(entity);
						MovementQueueComponent vmqc = Mappers.MQU_CM.get(ac.victim);

						Vector2Integer entityCurrentPosition = (mqc.currentMove != null) ? mqc.currentMove : TiledUtils.positionToTiled(Mappers.POS_CM.get(entity).position);
						Vector2Integer victimCurrentPosition = (vmqc.currentMove != null) ? vmqc.currentMove : TiledUtils.positionToTiled(Mappers.POS_CM.get(ac.victim).position);

						int range = 1;
						int distance = Vector2Integer.chebyshevDistance(entityCurrentPosition, victimCurrentPosition);
						boolean canSee = Util.raycast(map, entityCurrentPosition.x, entityCurrentPosition.y, victimCurrentPosition.x, victimCurrentPosition.y);

						if (distance <= range && canSee) {
							StateComponent sc = Mappers.STA_CM.get(entity);

							// Let the attacker to complete current move
							if (sc.state != EntityState.COMBAT && sc.state != EntityState.MOVING) {
								sc.state = EntityState.COMBAT;
							}

							StateComponent vsc = Mappers.STA_CM.get(ac.victim);

							// Let the victim to complete current move && change state so random walking stops
							if (vsc.state != EntityState.COMBAT && vsc.state != EntityState.MOVING) {
								vsc.state = EntityState.COMBAT;
							}

							if (Mappers.FOL_CM.get(ac.victim) == null) {
								// If not following then clear queue (Random walk)
								if (!vmqc.queue.isEmpty()) {
									mqc.queue.clear();
								}
								ac.victim.add(new FollowComponent(entity));
							}

							int damage = 1;

							vhc.health -= Util.clamp(damage, 0, vhc.maxHealth);

							AttackComponent vac = Mappers.ATT_CM.get(ac.victim);

							if (vac.victim == null) {
								vac.victim = entity;
							}

							if (!vac.attackers.contains(entity)) {
								vac.attackers.add(entity);
							}

							if (vhc.health == 0) {
								System.out.println("NPC EXEC");
								//NPCComponent nc = Mappers.NPC_CM.get(ac.victim);

								WorldMap worldMap = World.getInstance().getMaps().get(Mappers.MAP_CM.get(ac.victim).mapId);

								PacketNPCKill packet = new PacketNPCKill();
								packet.instanceId = Mappers.NPC_CM.get(ac.victim).instanceId;

								for (Entity e : worldMap.getPlayers()) {
									Mappers.CON_CM.get(e).connection.sendTCP(packet);
								}

								// FIXME Loot drop NPC
								//							for (Entity attacker : vac.attackers) {
								//								if (Mappers.SES_CM.get(attacker).session.isOpen()) {
								//									if (Vector2Integer.chebyshevDistance(victimCurrentPosition, TiledUtils.positionToTiled(Mappers.POS_CM.get(attacker).position)) <= 10) {
								//										World.getInstance().createDrop(attacker, nc.id, vpc.position.x, vpc.position.y);
								//									}
								//								}
								//							}

								// TODO Reset random walk counter???
								vac.victim = null;
								vac.attackers.clear();

								ac.victim.remove(FollowComponent.class);
								ac.victim = null;

								// Clear attackers mqc since victim is dead
								if (!mqc.queue.isEmpty()) {
									mqc.queue.clear();
								}

								// Set attacker state to IDLE if state is combat
								if (sc.state == EntityState.COMBAT) {
									sc.state = EntityState.IDLE;
								}

								// Remove attackers follow component
								entity.remove(FollowComponent.class);
							}

						}
					} else {
						// NPC was killed by somebody else.
						killSteal(entity, ac);
					}
					break;
				}
				case PLAYER: {
					if (!Mappers.CON_CM.get(ac.victim).connection.isConnected()) {
						// Stop xombat victim xlogged
						ac.victim = null;
						// FIXME STATE?
					} else {
						// Do cumbat NPC.
						// atacker either is player or NPC

						HealthComponent vhc = Mappers.HP_CM.get(ac.victim);

						if (vhc.health > 0) {
							MovementQueueComponent mqc = Mappers.MQU_CM.get(entity);
							MovementQueueComponent vmqc = Mappers.MQU_CM.get(ac.victim);

							Vector2Integer entityCurrentPosition = (mqc.currentMove != null) ? mqc.currentMove : TiledUtils.positionToTiled(Mappers.POS_CM.get(entity).position);
							Vector2Integer victimCurrentPosition = (vmqc.currentMove != null) ? vmqc.currentMove : TiledUtils.positionToTiled(Mappers.POS_CM.get(ac.victim).position);

							int range = 1;
							int distance = Vector2Integer.chebyshevDistance(entityCurrentPosition, victimCurrentPosition);
							boolean canSee = Util.raycast(map, entityCurrentPosition.x, entityCurrentPosition.y, victimCurrentPosition.x, victimCurrentPosition.y);

							if (distance <= range && canSee) {
								StateComponent sc = Mappers.STA_CM.get(entity);

								if (sc.state != EntityState.COMBAT && sc.state != EntityState.MOVING) {
									sc.state = EntityState.COMBAT;
								}

								int damage = 1;

								vhc.health -= Util.clamp(damage, 0, vhc.maxHealth);

								AttackComponent vac = Mappers.ATT_CM.get(ac.victim);

								if (!vac.attackers.contains(entity)) {
									vac.attackers.add(entity);
								}

								if (vhc.health == 0) {
									// Player killed

									System.out.println("Player DED");

									vac.victim = null;
									vac.attackers.clear();

									World.reset(ac.victim);

									// Clear attackers mqc since victim is dead
									if (!mqc.queue.isEmpty()) {
										mqc.queue.clear();
									}

									// Set attacker state to IDLE if state is combat
									if (sc.state == EntityState.COMBAT) {
										sc.state = EntityState.IDLE;
									}

									// Remove attackers follow component
									entity.remove(FollowComponent.class);
								}

							}

						} else {
							// Player victim was killed by someone else
							killSteal(entity, ac);
						}
					}
					break;
				}
				}


			} else {
				// FIXME we is dead
				ac.victim = null;
			}
		}


	}

	// When Entity was not killed by us
	private static void killSteal(Entity entity, AttackComponent attackComponent) {
		// Victim killed by sumbodi else
		attackComponent.victim = null;

		MovementQueueComponent mqc = Mappers.MQU_CM.get(entity);

		// Clear attackers mqc since victim is dead
		if (!mqc.queue.isEmpty()) {
			mqc.queue.clear();
		}

		StateComponent sc = Mappers.STA_CM.get(entity);

		// Set attacker state to IDLE if state is combat
		if (sc.state == EntityState.COMBAT) {
			sc.state = EntityState.IDLE;
		}

		// Remove attackers follow component
		entity.remove(FollowComponent.class);
	}

}