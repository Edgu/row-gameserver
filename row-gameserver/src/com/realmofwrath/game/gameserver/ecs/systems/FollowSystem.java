package com.realmofwrath.game.gameserver.ecs.systems;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.realmofwrath.game.custom.Map;
import com.realmofwrath.game.custom.Node;
import com.realmofwrath.game.custom.TiledUtils;
import com.realmofwrath.game.custom.Util;
import com.realmofwrath.game.custom.Vector2Integer;
import com.realmofwrath.game.gameserver.ecs.Mappers;
import com.realmofwrath.game.gameserver.ecs.components.AttackComponent;
import com.realmofwrath.game.gameserver.ecs.components.ConnectionComponent;
import com.realmofwrath.game.gameserver.ecs.components.FollowComponent;
import com.realmofwrath.game.gameserver.ecs.components.MovementQueueComponent;
import com.realmofwrath.game.gameserver.network.World;

public class FollowSystem extends IteratingSystem {

	public FollowSystem() {
		super(Family.all(FollowComponent.class).get());
	}

	@Override
	protected void processEntity(Entity entity, float deltaTime) {
		FollowComponent fc = Mappers.FOL_CM.get(entity);
		ConnectionComponent scc = Mappers.CON_CM.get(fc.subject);
		
		if (scc != null && !scc.connection.isConnected()) {
			System.out.println("REMOVE FOLLOW COMPONENT");
			entity.remove(FollowComponent.class);
		} else {
			MovementQueueComponent mqc = Mappers.MQU_CM.get(entity);
			Vector2Integer entityCurrentPosition = TiledUtils.positionToTiled(Mappers.POS_CM.get(entity).position);
			Vector2Integer entityPosition = (mqc.currentMove != null) ? mqc.currentMove : entityCurrentPosition;

			MovementQueueComponent smqc = Mappers.MQU_CM.get(fc.subject);
			Vector2Integer subjectCurrentPosition = TiledUtils.positionToTiled(Mappers.POS_CM.get(fc.subject).position);
			Vector2Integer subjectPosition = (smqc.currentMove != null) ? smqc.currentMove : subjectCurrentPosition;

			AttackComponent ac = Mappers.ATT_CM.get(entity);

			int distance = Vector2Integer.chebyshevDistance(entityPosition, subjectPosition);

			Map map = World.getInstance().getMaps().get(Mappers.MAP_CM.get(entity).mapId);
			
			if (fc.lastKnownSubjectPosition == null) {
				fc.recalculate = true;
			} else if (!fc.lastKnownSubjectPosition.equals(subjectPosition)) {
				fc.recalculate = true;
			}
			
			if (ac.victim != null) {
				// Combat following
				boolean canSee = Util.raycast(map, entityPosition.x, entityPosition.y, subjectPosition.x, subjectPosition.y);

				if (distance <= fc.range && canSee) {
					if (!mqc.queue.isEmpty()) {
						mqc.queue.clear();
						fc.recalculate = false;
					}
				} else if ((distance > fc.range || !canSee) && mqc.currentMove == null) {
					List<Node> path = Util.findPath(map, entityPosition, subjectPosition, true, false);
					if (!path.isEmpty()) {
						List<Node> removed = new ArrayList<Node>();

						for (int i = 0; i < fc.range; i++) {
							removed.add(path.remove(0));
						}

						Collections.reverse(removed);

						if (!path.isEmpty()) {
							Node finalNode = path.get(0);
							canSee = Util.raycast(map, finalNode.position.x, finalNode.position.y, subjectPosition.x, subjectPosition.y);

							// If can't see, check through the removed nodes.
							if (!canSee) {
								for (Node node : removed) {
									path.add(0, node);
									canSee = Util.raycast(map, node.position.x, node.position.y, subjectPosition.x, subjectPosition.y);
									if (canSee) {
										if (fc.lastKnownSubjectPosition == null) {
											fc.lastKnownSubjectPosition = new Vector2Integer(subjectPosition);
										} else {
											fc.lastKnownSubjectPosition.set(subjectPosition);
										}
										fc.recalculate = false;
										MovementSystem.overwritePath(mqc, path);
									}
								}
							} else {
								if (fc.lastKnownSubjectPosition == null) {
									fc.lastKnownSubjectPosition = new Vector2Integer(subjectPosition);
								} else {
									fc.lastKnownSubjectPosition.set(subjectPosition);
								}
								fc.recalculate = false;
								MovementSystem.overwritePath(mqc, path);
							}
						}
					} else {
						System.err.println("FOLLOW_SYSTEM: NO PATH FOUND - COMBAT");
						entity.remove(FollowComponent.class);
					}
				}

			} else {
				if (distance <= fc.range) {
					if (!mqc.queue.isEmpty()) {
						mqc.queue.clear();
						fc.recalculate = false;
					}
				} else if (distance > fc.range && fc.recalculate) {
					List<Node> path = Util.findPath(map, entityPosition, subjectPosition, true, false);

					if (path.size() > 1) {
						if (fc.lastKnownSubjectPosition == null) {
							fc.lastKnownSubjectPosition = new Vector2Integer(subjectPosition);
						} else {
							fc.lastKnownSubjectPosition.set(subjectPosition);
						}
						fc.recalculate = false;
						path.remove(0);
						MovementSystem.overwritePath(mqc, path);
					} else {
						System.err.println("FOLLOW_SYSTEM: NO PATH FOUND - NON COMBAT");
						entity.remove(FollowComponent.class);
					}
				}
			}
		}

	}

}