package com.realmofwrath.game.gameserver.ecs.systems;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.realmofwrath.game.custom.Constants;
import com.realmofwrath.game.gameserver.ecs.Mappers;
import com.realmofwrath.game.gameserver.ecs.components.IdComponent;
import com.realmofwrath.game.gameserver.ecs.components.InventoryComponent;
import com.realmofwrath.game.gameserver.ecs.components.SlotComponent;
import com.realmofwrath.game.gameserver.ecs.components.items.ItemComponent;
import com.realmofwrath.game.gameserver.ecs.entities.Item;
import com.realmofwrath.game.gameserver.network.World;
import com.realmofwrath.game.gameserver.sql.Database;
import com.realmofwrath.game.inventory.ItemType;
import com.realmofwrath.game.network.packet.inventory.PacketInventorySlotRemove;
import com.realmofwrath.game.network.packet.inventory.PacketInventorySlotUpdate;

public abstract class InventorySystem extends EntitySystem {

	public static SlotComponent[] createPlayerInventorySlots() {
		SlotComponent[] slots = new SlotComponent[Constants.INVENTORY_SLOTS_AMOUNT];
		for (int i = 0; i < slots.length; i++) {
			ItemType tempType;
			if (i < Constants.INVENTORY_EQUIPMENT_SLOTS_AMOUNT) {
				tempType = ItemType.getType(i + 1);
			} else {
				tempType = ItemType.DEFAULT;
			}
			slots[i] = new SlotComponent(tempType);
		}
		return slots;
	}

	public static boolean isInventoryFull(Entity entity) {
		return getFirstFreeInventorySlot(entity) != -1;
	}

	public static int getFirstFreeInventorySlot(Entity entity) {
		InventoryComponent ic = Mappers.INV_CM.get(entity);
		// Offset gear slots (weapon, armor, etc...)
		for (int i = Constants.INVENTORY_EQUIPMENT_SLOTS_AMOUNT; i < ic.slots.length; i++) {
			if (ic.slots[i].item == null) {
				return i;
			}
		}
		return -1;
	}

	public static boolean setSlot(Entity entity, int slotId, Item item) {
		InventoryComponent inventoryComponent = Mappers.INV_CM.get(entity);
		if (inventoryComponent != null) {
			if (slotId > 0 && slotId < Constants.INVENTORY_SLOTS_AMOUNT) {
				SlotComponent slotComponent = inventoryComponent.slots[slotId];
				IdComponent entityIdComponent = Mappers.ID_CM.get(entity);
				ItemComponent itemComponent = Mappers.ITE_CM.get(item);
				IdComponent itemIdComponent = Mappers.ID_CM.get(item);
				if (item == null) {
					slotComponent.item = item;
					PacketInventorySlotRemove packet = new PacketInventorySlotRemove();
					packet.slotId = slotId;
					Mappers.CON_CM.get(entity).connection.sendTCP(packet);
					return Database.removeItem(entityIdComponent.id, slotId);
				} else if (itemComponent.itemType == ItemType.DEFAULT || itemComponent.itemType == slotComponent.type) {
					slotComponent.item = item;
					PacketInventorySlotUpdate packet = new PacketInventorySlotUpdate();
					packet.slotId = slotId;
					packet.itemId = itemIdComponent.id;
					Mappers.CON_CM.get(entity).connection.sendTCP(packet);
					return Database.giveItem(entityIdComponent.id, slotId, itemIdComponent.id);
				}
			}
		}
		return false;
	}

	public static boolean setSlot(Entity entity, int slotId, int itemId) {
		return setSlot(entity, slotId, World.getItem(itemId));
	}

	public static boolean setFirstFreeSlot(Entity entity, int itemId) {
		return setSlot(entity, getFirstFreeInventorySlot(entity), itemId);
	}

	public static boolean setFirstFreeSlot(Entity entity, Item item) {
		return setSlot(entity, getFirstFreeInventorySlot(entity), item);
	}

	public static boolean clearSlot(Entity entity, int slotId) {
		return setSlot(entity, slotId, null);
	}

}