package com.realmofwrath.game.gameserver.ecs.systems;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IntervalSystem;
import com.badlogic.ashley.utils.ImmutableArray;
import com.realmofwrath.game.gameserver.GameServer;
import com.realmofwrath.game.gameserver.ecs.Mappers;
import com.realmofwrath.game.gameserver.ecs.components.DropBagComponent;

public class LootSystem extends IntervalSystem {

	private ImmutableArray<Entity> entities;

	public LootSystem(int interval) {
		super(interval);
	}

	@Override
	public void addedToEngine(Engine engine) {
		entities = engine.getEntitiesFor(Family.all(DropBagComponent.class).get());
	}

	// FIXME LOOTSYSTEM
	@Override
	protected void updateInterval() {
		for (Entity entity : entities) {
			DropBagComponent dbc = Mappers.DB_CM.get(entity);

			if (dbc.owner != null) {
				if (dbc.counter >= GameServer.DROP_PUBLIC_OWNERSHIP) {
					dbc.owner = null;
					//					World.getInstance().sendDrop(entity, null);
				}
			}

			if (dbc.counter >= GameServer.DROP_REMOVE) {
				//				World.getInstance().deleteDrop(entity);
			}

			dbc.counter++;
		}
	}

}