package com.realmofwrath.game.gameserver.ecs.systems;

import java.util.List;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.realmofwrath.game.custom.Constants;
import com.realmofwrath.game.custom.Node;
import com.realmofwrath.game.custom.TiledUtils;
import com.realmofwrath.game.custom.Util;
import com.realmofwrath.game.custom.Vector2Integer;
import com.realmofwrath.game.gameserver.custom.WorldMap;
import com.realmofwrath.game.gameserver.ecs.Mappers;
import com.realmofwrath.game.gameserver.ecs.components.MapComponent;
import com.realmofwrath.game.gameserver.ecs.components.MovementQueueComponent;
import com.realmofwrath.game.gameserver.ecs.components.PositionComponent;
import com.realmofwrath.game.gameserver.ecs.components.VelocityComponent;
import com.realmofwrath.game.gameserver.ecs.entities.EntityState;
import com.realmofwrath.game.gameserver.network.World;
import com.realmofwrath.game.network.packet.PacketMove;
import com.realmofwrath.game.network.packet.player.PacketNewPlayer;

public class MovementSystem extends IteratingSystem {

	public MovementSystem() {
		super(Family.all(PositionComponent.class, VelocityComponent.class, MovementQueueComponent.class).get());
	}

	@Override
	protected void processEntity(Entity entity, float deltaTime) {
		if (Mappers.HP_CM.get(entity).health > 0) {
			EntityState state = Mappers.STA_CM.get(entity).state;
			MovementQueueComponent mqc = Mappers.MQU_CM.get(entity);
			if (state == EntityState.MOVING || !mqc.queue.isEmpty()) {
				Vector2Integer position = Mappers.POS_CM.get(entity).position;
				Vector2Integer velocity = Mappers.VEL_CM.get(entity).velocity;

				if (mqc.currentMove != null) {
					Vector2Integer mapPosition = TiledUtils.positionToMap(mqc.currentMove.x, mqc.currentMove.y);
					if (velocity.x != 0) {
						if (mapPosition.x == position.x) {
							velocity.x = 0;
						}
					}

					if (velocity.y != 0) {
						if (mapPosition.y == position.y) {
							velocity.y = 0;
						}
					}

					if (mapPosition.equals(position)) {
						MapComponent mc = Mappers.MAP_CM.get(entity);
						WorldMap currentMap = World.getInstance().getMaps().get(mc.mapId);

						// Standing on warp tile.
						TiledMapTile tile = TiledUtils.getTile(currentMap.getMainLayer(), mqc.currentMove);

						mqc.currentMove = null;

						WARP: {
							if (tile.getProperties().containsKey("name")) {
								if (tile.getProperties().get("name").equals("warp")) {

									// Get WARP object
									for (MapObject mapObject : currentMap.getObjectLayer().getObjects()) {
										Vector2Integer warpTile = new Vector2Integer(mapObject.getProperties().get("x", Float.class).intValue(), mapObject.getProperties().get("y", Float.class).intValue());
										if (warpTile.equals(mapPosition)) {
											int mapId = Integer.parseInt(mapObject.getProperties().get("destinationMap").toString());
											int objectId = Integer.parseInt(mapObject.getProperties().get("destinationObjectId").toString());

											WorldMap destinationMap = World.getInstance().getMaps().get(mapId);

											for (MapObject warpMapObject : destinationMap.getObjectLayer().getObjects()) {
												int warpMapObjectId = Integer.parseInt(warpMapObject.getProperties().get("id").toString());

												if (warpMapObjectId == objectId) {
													System.out.println("INITIATE WARP TO MAP: " + mapId);

													position.x = warpMapObject.getProperties().get("x", Float.class).intValue();
													position.y = warpMapObject.getProperties().get("y", Float.class).intValue();

													currentMap.removePlayer(entity);
													
													Vector2Integer sendMove = (mqc.currentMove != null) ? mqc.currentMove : TiledUtils.positionToTiled(Mappers.POS_CM.get(entity).position);
													
													PacketNewPlayer packet = new PacketNewPlayer();
													packet.id = Mappers.ID_CM.get(entity).id;
													packet.x = sendMove.x;
													packet.y = sendMove.y;
													
													destinationMap.sendToAll(packet);
													destinationMap.addPlayer(entity);

													mc.mapId = mapId;

													mqc.queue.clear();
													break WARP;
												}
											}
										}
									}
								}
							}
						}

						Mappers.STA_CM.get(entity).state = EntityState.IDLE;
					}
				}

				if (!mqc.queue.isEmpty()) {
					if (mqc.currentMove == null) {
						Vector2Integer nextPosition = nextMove(mqc);
						Vector2Integer tiledPosition = TiledUtils.positionToTiled(position);
						velocity.x = nextPosition.x - tiledPosition.x;
						velocity.y = nextPosition.y - tiledPosition.y;

						int id = -1;
						int type = 1;

						if (Mappers.CON_CM.get(entity) != null) {
							id = Mappers.ID_CM.get(entity).id;
						} else {
							id = Mappers.NPC_CM.get(entity).instanceId;
							type = 0;
						}

						WorldMap worldMap = World.getInstance().getMaps().get(Mappers.MAP_CM.get(entity).mapId);

						int direction = Util.velocityToDirection(velocity).getValue();

						PacketMove packet = new PacketMove();
						packet.id = id;
						packet.direction = direction;
						packet.type = type;
						
						worldMap.sendToAll(packet);

						Mappers.STA_CM.get(entity).state = EntityState.MOVING;
					}
				}

				position.x += velocity.x * Constants.MOVEMENT_SPEED;
				position.y += velocity.y * Constants.MOVEMENT_SPEED;
			}
		}
	}

	public static void insertPath(MovementQueueComponent mqc, List<Node> path) {
		for (int i = path.size() - 1; i >= 0; i--) {
			mqc.queue.add(path.get(i).position);
		}
	}

	public static void overwritePath(MovementQueueComponent mqc, List<Node> path) {
		mqc.queue.clear();
		insertPath(mqc, path);
	}

	private static Vector2Integer nextMove(MovementQueueComponent mqc) {
		return mqc.currentMove = mqc.queue.poll();
	}

}