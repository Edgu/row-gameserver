package com.realmofwrath.game.gameserver.ecs.systems;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IntervalSystem;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.realmofwrath.game.custom.Node;
import com.realmofwrath.game.custom.TiledUtils;
import com.realmofwrath.game.custom.Util;
import com.realmofwrath.game.custom.Vector2Integer;
import com.realmofwrath.game.gameserver.custom.WorldMap;
import com.realmofwrath.game.gameserver.ecs.Mappers;
import com.realmofwrath.game.gameserver.ecs.components.HealthComponent;
import com.realmofwrath.game.gameserver.ecs.components.MovementQueueComponent;
import com.realmofwrath.game.gameserver.ecs.components.RandomWalkComponent;
import com.realmofwrath.game.gameserver.ecs.components.StateComponent;
import com.realmofwrath.game.gameserver.ecs.entities.EntityState;
import com.realmofwrath.game.gameserver.network.World;

public class RandomWalkSystem extends IntervalSystem {

	private ImmutableArray<Entity> entities;

	public RandomWalkSystem (float interval){
		super(interval);
	}

	@Override
	public void addedToEngine(Engine engine) {
		entities = engine.getEntitiesFor(Family.all(RandomWalkComponent.class).get());
	}

	@Override
	protected void updateInterval() {
		for (Entity entity : entities) {
			HealthComponent hc = Mappers.HP_CM.get(entity);
			if (hc.health > 0) {
				StateComponent sc = Mappers.STA_CM.get(entity);
				if (sc.state == EntityState.IDLE) {
					MovementQueueComponent mqc = Mappers.MQU_CM.get(entity);
					Vector2Integer currentMove = mqc.currentMove;
					Vector2Integer start = (currentMove != null) ? currentMove : TiledUtils.positionToTiled(Mappers.POS_CM.get(entity).position);

					RandomWalkComponent rwc = Mappers.RWA_CM.get(entity);
					Vector2Integer destination;

					do {
						destination = rwc.area.get(ThreadLocalRandom.current().nextInt(rwc.area.size()));
					} while (start.equals(destination));

					List<Node> path = Util.findPath(World.getInstance().getMaps().get(Mappers.MAP_CM.get(entity).mapId), start, destination, true, false);

					if (!path.isEmpty()) {
						MovementSystem.overwritePath(mqc, path);
					}
				}
			}
		}
	}

	public static List<Vector2Integer> getRandomWalkArea(WorldMap worldMap, int x, int y, int radius) {
		List<Vector2Integer> area = new ArrayList<Vector2Integer>();

		Vector2Integer initialPosition = TiledUtils.positionToTiled(x, y);

		for (int yy = initialPosition.y - radius; yy <= initialPosition.y + radius; yy++) {
			for (int xx = initialPosition.x - radius; xx <= initialPosition.x + radius; xx++) {
				TiledMapTile tile = TiledUtils.getTile(worldMap.getMainLayer(), xx, yy);
				if (tile != null) {
					if (!TiledUtils.isTileSolid(tile)) {
						TiledMapTile objectTile = TiledUtils.getTile(worldMap.getTileObjectLayer(), xx, yy);
						if (objectTile == null || !TiledUtils.isTileSolid(objectTile)) {

							ADD: {
								// check for warps n shit
								for (MapObject mapObject : worldMap.getObjectLayer().getObjects()) {
									Vector2Integer tilePosition = TiledUtils.positionToTiled(mapObject.getProperties().get("x", Float.class).intValue(), mapObject.getProperties().get("y", Float.class).intValue());
	
									if (tilePosition.equals(xx, yy)) {
										break ADD;
									}
								}
	
								area.add(new Vector2Integer(xx, yy));
							}
						}
					}
				}
			}
		}

		return area;
	}

}