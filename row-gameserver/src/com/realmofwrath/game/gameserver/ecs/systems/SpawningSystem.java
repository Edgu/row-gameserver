package com.realmofwrath.game.gameserver.ecs.systems;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IntervalSystem;
import com.badlogic.ashley.utils.ImmutableArray;
import com.realmofwrath.game.custom.TiledUtils;
import com.realmofwrath.game.custom.Vector2Integer;
import com.realmofwrath.game.gameserver.custom.WorldMap;
import com.realmofwrath.game.gameserver.ecs.Mappers;
import com.realmofwrath.game.gameserver.ecs.components.HealthComponent;
import com.realmofwrath.game.gameserver.ecs.components.MovementQueueComponent;
import com.realmofwrath.game.gameserver.ecs.components.NPCComponent;
import com.realmofwrath.game.gameserver.ecs.components.PositionComponent;
import com.realmofwrath.game.gameserver.ecs.components.SpawnComponent;
import com.realmofwrath.game.gameserver.ecs.components.VelocityComponent;
import com.realmofwrath.game.gameserver.ecs.entities.EntityState;
import com.realmofwrath.game.gameserver.network.World;
import com.realmofwrath.game.network.packet.npc.PacketNPCSpawn;

public class SpawningSystem extends IntervalSystem {

	private ImmutableArray<Entity> entities;

	public SpawningSystem(int interval) {
		super(interval);
	}

	@Override
	public void addedToEngine(Engine engine) {
		entities = engine.getEntitiesFor(Family.all(SpawnComponent.class).get());
	}

	@Override
	protected void updateInterval() {
		for (Entity entity : entities) {
			HealthComponent hc = Mappers.HP_CM.get(entity);

			if (hc.health == 0) {
				SpawnComponent sc = Mappers.SPAWN_CM.get(entity);
				sc.counter++;
				if (sc.counter >= sc.respawnTime) {
					PositionComponent pc = Mappers.POS_CM.get(entity);
					NPCComponent nc = Mappers.NPC_CM.get(entity);

					// reset stuff
					VelocityComponent vc = Mappers.VEL_CM.get(entity);
					MovementQueueComponent mqc = Mappers.MQU_CM.get(entity);

					vc.velocity.x = 0;
					vc.velocity.y = 0;
					mqc.currentMove = null;
					mqc.queue.clear();

					hc.health = hc.maxHealth;
					pc.position.x = sc.x;
					pc.position.y = sc.y;
					sc.counter = 0;
					Vector2Integer position = TiledUtils.positionToTiled(pc.position);
					Mappers.STA_CM.get(entity).state = EntityState.IDLE;

					WorldMap worldMap = World.getInstance().getMaps().get(Mappers.MAP_CM.get(entity).mapId);

					PacketNPCSpawn packet = new PacketNPCSpawn();
					packet.id = nc.id;
					packet.instanceId = nc.instanceId;
					packet.x = position.x;
					packet.y = position.y;
					packet.health = hc.health;
					packet.maxHealth = hc.maxHealth;

					for (Entity player : worldMap.getPlayers()) {
						Mappers.CON_CM.get(player).connection.sendTCP(packet);
					}
				}
			}
		}

	}

}