package com.realmofwrath.game.gameserver.ecs.systems.skills;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.systems.IntervalSystem;
import com.badlogic.ashley.utils.ImmutableArray;
import com.realmofwrath.game.gameserver.ecs.Mappers;
import com.realmofwrath.game.gameserver.ecs.components.skills.ReplenishComponent;

public abstract class ResourceReplenishSystem extends IntervalSystem {

	protected ImmutableArray<Entity> entities;

	public ResourceReplenishSystem(int interval) {
		super(interval);
	}

	public abstract void addedToEngine(Engine engine);

	@Override
	protected void updateInterval() {
		for (Entity entity : entities) {
			ReplenishComponent rc = Mappers.REP_CM.get(entity);
			if (rc.seconds == 0) {
				replenish(entity);
			} else {
				rc.seconds--;
			}
		}
	}

	public abstract void replenish(Entity entity);

}