package com.realmofwrath.game.gameserver.ecs.systems.skills.fishing;

import java.util.concurrent.ThreadLocalRandom;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.realmofwrath.game.custom.TiledUtils;
import com.realmofwrath.game.custom.Vector2Integer;
import com.realmofwrath.game.gameserver.custom.WorldMap;
import com.realmofwrath.game.gameserver.ecs.Mappers;
import com.realmofwrath.game.gameserver.ecs.components.MapComponent;
import com.realmofwrath.game.gameserver.ecs.components.skills.ReplenishComponent;
import com.realmofwrath.game.gameserver.ecs.components.skills.fishing.FishableComponent;
import com.realmofwrath.game.gameserver.ecs.systems.skills.ResourceReplenishSystem;
import com.realmofwrath.game.gameserver.network.World;
import com.realmofwrath.game.network.packet.skill.fishing.PacketFishReplenish;

public class FishReplenishSystem extends ResourceReplenishSystem {

	public FishReplenishSystem(int interval) {
		super(interval);
	}

	@Override
	public void addedToEngine(Engine engine) {
		entities = engine.getEntitiesFor(Family.all(ReplenishComponent.class, FishableComponent.class).get());
	}

	@Override
	public void replenish(Entity entity) {
		Vector2Integer position = TiledUtils.positionToTiled(Mappers.POS_CM.get(entity).position);
		TiledUtils.setTile(World.getInstance().getMaps().get(Mappers.MAP_CM.get(entity).mapId).getTileObjectLayer(), TiledUtils.FISH, position);
		entity.remove(ReplenishComponent.class);
		FishableComponent fc = Mappers.FAB_CM.get(entity);
		fc.resources = ThreadLocalRandom.current().nextInt(fc.min, fc.max + 1);

		MapComponent mapComponent = Mappers.MAP_CM.get(entity);
		WorldMap worldMap = World.getInstance().getMaps().get(mapComponent.mapId);

		PacketFishReplenish packet = new PacketFishReplenish();
		packet.x = position.x;
		packet.y = position.y;

		for (Entity e : worldMap.getPlayers()) {
			Mappers.CON_CM.get(e).connection.sendTCP(packet);
		}
	}

}