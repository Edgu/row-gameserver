package com.realmofwrath.game.gameserver.ecs.systems.skills.fishing;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IntervalSystem;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.realmofwrath.game.custom.Map;
import com.realmofwrath.game.custom.TiledUtils;
import com.realmofwrath.game.custom.Vector2Integer;
import com.realmofwrath.game.gameserver.custom.WorldMap;
import com.realmofwrath.game.gameserver.ecs.Mappers;
import com.realmofwrath.game.gameserver.ecs.components.MapComponent;
import com.realmofwrath.game.gameserver.ecs.components.StateComponent;
import com.realmofwrath.game.gameserver.ecs.components.skills.ReplenishComponent;
import com.realmofwrath.game.gameserver.ecs.components.skills.fishing.FishableComponent;
import com.realmofwrath.game.gameserver.ecs.components.skills.fishing.FishingComponent;
import com.realmofwrath.game.gameserver.ecs.entities.EntityState;
import com.realmofwrath.game.gameserver.ecs.entities.environment.FishingSpot;
import com.realmofwrath.game.gameserver.network.World;
import com.realmofwrath.game.network.packet.skill.fishing.PacketFishDepleted;

public class FishingSystem extends IntervalSystem {

	private ImmutableArray<Entity> entities;

	public FishingSystem(int interval) {
		super(interval);
	}

	@Override
	public void addedToEngine(Engine engine) {
		entities = engine.getEntitiesFor(Family.all(FishingComponent.class).get());
	}

	@Override
	protected void updateInterval() {
		for (Entity entity : entities) {
			FishingComponent fc = Mappers.FIS_CM.get(entity);
			StateComponent sc = Mappers.STA_CM.get(entity);
			if (sc.state == EntityState.FISHING) {
				FishableComponent cc = Mappers.FAB_CM.get(fc.interactee);
				if (cc.resources > 0) {
					cc.resources--; // XXX CHANCE ???
					// TODO GIVE <>< TO PLAYER
					System.out.println("<><");
				} else {
					entity.remove(FishingComponent.class);
					sc.state = EntityState.IDLE;
					Map map = World.getInstance().getMaps().get(Mappers.MAP_CM.get(entity).mapId);
					fish(map.getTileObjectLayer(), fc.interactee);
				}
			} else if (fc.interactee != null) {
				if (sc.state == EntityState.IDLE) {
					int distance = Vector2Integer.chebyshevDistance(TiledUtils.positionToTiled(Mappers.POS_CM.get(entity).position), TiledUtils.positionToTiled(Mappers.POS_CM.get(fc.interactee).position));
					if (distance == 1) {
						sc.state = EntityState.FISHING;
					}
				}
			}
		}
	}

	private static void fish(TiledMapTileLayer layer, Entity entity) {
		Vector2Integer position = TiledUtils.positionToTiled(Mappers.POS_CM.get(entity).position);
		TiledUtils.setTile(layer, TiledUtils.FISH_DEPLETED, position);
		entity.add(new ReplenishComponent(FishingSpot.REPLENISH_TIME));

		MapComponent mc = Mappers.MAP_CM.get(entity);
		WorldMap worldMap = World.getInstance().getMaps().get(mc.mapId);

		PacketFishDepleted packet = new PacketFishDepleted();
		packet.x = position.x;
		packet.y = position.y;

		for (Entity e : worldMap.getPlayers()) {
			Mappers.CON_CM.get(e).connection.sendTCP(packet);
		}
	}

}