package com.realmofwrath.game.gameserver.ecs.systems.skills.mining;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IntervalSystem;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.realmofwrath.game.custom.Map;
import com.realmofwrath.game.custom.TiledUtils;
import com.realmofwrath.game.custom.Vector2Integer;
import com.realmofwrath.game.gameserver.custom.WorldMap;
import com.realmofwrath.game.gameserver.ecs.Mappers;
import com.realmofwrath.game.gameserver.ecs.components.MapComponent;
import com.realmofwrath.game.gameserver.ecs.components.StateComponent;
import com.realmofwrath.game.gameserver.ecs.components.skills.ReplenishComponent;
import com.realmofwrath.game.gameserver.ecs.components.skills.mining.MinableComponent;
import com.realmofwrath.game.gameserver.ecs.components.skills.mining.MiningComponent;
import com.realmofwrath.game.gameserver.ecs.entities.EntityState;
import com.realmofwrath.game.gameserver.ecs.entities.environment.Rock;
import com.realmofwrath.game.gameserver.network.World;
import com.realmofwrath.game.network.packet.skill.mining.PacketRockMined;

public class MiningSystem extends IntervalSystem {

	private ImmutableArray<Entity> entities;

	public MiningSystem(int interval) {
		super(interval);
	}

	@Override
	public void addedToEngine(Engine engine) {
		entities = engine.getEntitiesFor(Family.all(MiningComponent.class).get());
	}

	@Override
	protected void updateInterval() {
		for (Entity entity : entities) {
			MiningComponent mc = Mappers.MIN_CM.get(entity);
			StateComponent sc = Mappers.STA_CM.get(entity);
			if (sc.state == EntityState.MINING) {
				MinableComponent cc = Mappers.MAB_CM.get(mc.interactee);
				if (cc.resources > 0) {
					cc.resources--; // XXX CHANCE ???
					// TODO GIVE ORES TO PLAYER
					System.out.println("MINE");
				} else {
					entity.remove(MiningComponent.class);
					sc.state = EntityState.IDLE;
					Map map = World.getInstance().getMaps().get(Mappers.MAP_CM.get(entity).mapId);
					mine(map.getTileObjectLayer(), mc.interactee);
				}
			} else if (mc.interactee != null) {
				if (sc.state == EntityState.IDLE) {
					int distance = Vector2Integer.chebyshevDistance(TiledUtils.positionToTiled(Mappers.POS_CM.get(entity).position), TiledUtils.positionToTiled(Mappers.POS_CM.get(mc.interactee).position));
					if (distance == 1) {
						sc.state = EntityState.MINING;
					}
				}
			}
		}
	}

	private static void mine(TiledMapTileLayer layer, Entity entity) {
		Vector2Integer position = TiledUtils.positionToTiled(Mappers.POS_CM.get(entity).position);
		TiledUtils.setTile(layer, TiledUtils.ROCK_MINED, position);
		entity.add(new ReplenishComponent(Rock.REPLENISH_TIME));

		MapComponent mc = Mappers.MAP_CM.get(entity);
		WorldMap worldMap = World.getInstance().getMaps().get(mc.mapId);

		PacketRockMined packet = new PacketRockMined();
		packet.x = position.x;
		packet.y = position.y;

		for (Entity e : worldMap.getPlayers()) {
			Mappers.CON_CM.get(e).connection.sendTCP(packet);
		}
	}

}