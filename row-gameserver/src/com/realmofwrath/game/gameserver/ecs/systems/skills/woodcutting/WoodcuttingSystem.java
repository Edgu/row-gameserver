package com.realmofwrath.game.gameserver.ecs.systems.skills.woodcutting;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IntervalSystem;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.realmofwrath.game.custom.Map;
import com.realmofwrath.game.custom.TiledUtils;
import com.realmofwrath.game.custom.Vector2Integer;
import com.realmofwrath.game.gameserver.custom.WorldMap;
import com.realmofwrath.game.gameserver.ecs.Mappers;
import com.realmofwrath.game.gameserver.ecs.components.MapComponent;
import com.realmofwrath.game.gameserver.ecs.components.StateComponent;
import com.realmofwrath.game.gameserver.ecs.components.skills.ReplenishComponent;
import com.realmofwrath.game.gameserver.ecs.components.skills.woodcutting.ChoppableComponent;
import com.realmofwrath.game.gameserver.ecs.components.skills.woodcutting.WoodcuttingComponent;
import com.realmofwrath.game.gameserver.ecs.entities.EntityState;
import com.realmofwrath.game.gameserver.ecs.entities.environment.Tree;
import com.realmofwrath.game.gameserver.network.World;
import com.realmofwrath.game.network.packet.skill.woodcutting.PacketTreeCutDown;

public class WoodcuttingSystem extends IntervalSystem {

	private ImmutableArray<Entity> entities;

	public WoodcuttingSystem(int interval) {
		super(interval);
	}

	@Override
	public void addedToEngine(Engine engine) {
		entities = engine.getEntitiesFor(Family.all(WoodcuttingComponent.class).get());
	}

	@Override
	protected void updateInterval() {
		for (Entity entity : entities) {
			WoodcuttingComponent wc = Mappers.WOO_CM.get(entity);
			StateComponent sc = Mappers.STA_CM.get(entity);
			if (sc.state == EntityState.WOODCUTTING) {
				ChoppableComponent cc = Mappers.CHO_CM.get(wc.interactee);
				if (cc.resources > 0) {
					cc.resources--; // XXX CHANCE ???
					// TODO GIVE LOGS TO PLAYER
					System.out.println("CHOP");
				} else {
					entity.remove(WoodcuttingComponent.class);
					sc.state = EntityState.IDLE;
					Map map = World.getInstance().getMaps().get(Mappers.MAP_CM.get(entity).mapId);
					cut(map.getTileObjectLayer(), wc.interactee);
				}
			} else if (wc.interactee != null) {
				if (sc.state == EntityState.IDLE) {
					int distance = Vector2Integer.chebyshevDistance(TiledUtils.positionToTiled(Mappers.POS_CM.get(entity).position), TiledUtils.positionToTiled(Mappers.POS_CM.get(wc.interactee).position));
					if (distance == 1) {
						sc.state = EntityState.WOODCUTTING;
					}
				}
			}
		}
	}

	private static void cut(TiledMapTileLayer layer, Entity entity) {
		Vector2Integer position = TiledUtils.positionToTiled(Mappers.POS_CM.get(entity).position);
		TiledUtils.setTile(layer, TiledUtils.TREE_STUMP, position);
		entity.add(new ReplenishComponent(Tree.TREE_REGROWTH_TIME));

		MapComponent mapComponent = Mappers.MAP_CM.get(entity);
		WorldMap worldMap = World.getInstance().getMaps().get(mapComponent.mapId);

		PacketTreeCutDown packet = new PacketTreeCutDown();
		packet.x = position.x;
		packet.y = position.y;

		for (Entity e : worldMap.getPlayers()) {
			Mappers.CON_CM.get(e).connection.sendTCP(packet);
		}
	}

}