package com.realmofwrath.game.gameserver.managers;

import static com.realmofwrath.game.gameserver.GameServer.FISHING_INTERVAL;
import static com.realmofwrath.game.gameserver.GameServer.FISH_REPLENISH_INTERVAL;
import static com.realmofwrath.game.gameserver.GameServer.LOOT_SYSTEM_INTERVAL;
import static com.realmofwrath.game.gameserver.GameServer.MINING_INTERVAL;
import static com.realmofwrath.game.gameserver.GameServer.RANDOM_MOVE_INTERVAL;
import static com.realmofwrath.game.gameserver.GameServer.ROCK_REPLENISH_INTERVAL;
import static com.realmofwrath.game.gameserver.GameServer.SPAWN_SYSTEM_INTERVAL;

import java.util.ArrayList;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;
import com.esotericsoftware.kryonet.Connection;
import com.realmofwrath.game.custom.Vector2Integer;
import com.realmofwrath.game.gameserver.data.DropBagData;
import com.realmofwrath.game.gameserver.ecs.Mappers;
import com.realmofwrath.game.gameserver.ecs.components.ConnectionComponent;
import com.realmofwrath.game.gameserver.ecs.components.IdComponent;
import com.realmofwrath.game.gameserver.ecs.components.NPCComponent;
import com.realmofwrath.game.gameserver.ecs.components.SlotComponent;
import com.realmofwrath.game.gameserver.ecs.systems.BattleSystem;
import com.realmofwrath.game.gameserver.ecs.systems.FollowSystem;
import com.realmofwrath.game.gameserver.ecs.systems.LootSystem;
import com.realmofwrath.game.gameserver.ecs.systems.MovementSystem;
import com.realmofwrath.game.gameserver.ecs.systems.RandomWalkSystem;
import com.realmofwrath.game.gameserver.ecs.systems.SpawningSystem;
import com.realmofwrath.game.gameserver.ecs.systems.skills.fishing.FishReplenishSystem;
import com.realmofwrath.game.gameserver.ecs.systems.skills.fishing.FishingSystem;
import com.realmofwrath.game.gameserver.ecs.systems.skills.mining.MiningSystem;
import com.realmofwrath.game.gameserver.ecs.systems.skills.mining.RockReplenishSystem;
import com.realmofwrath.game.gameserver.ecs.systems.skills.woodcutting.TreeRegrowthSystem;
import com.realmofwrath.game.gameserver.ecs.systems.skills.woodcutting.WoodcuttingSystem;
import com.realmofwrath.game.gameserver.network.World;

public class EntityManager extends Engine {

	private static final EntityManager INSTANCE = new EntityManager();

	private EntityManager() {
		addSystem(new MovementSystem());
		addSystem(new TreeRegrowthSystem(ROCK_REPLENISH_INTERVAL));
		addSystem(new WoodcuttingSystem(MINING_INTERVAL));

		addSystem(new RockReplenishSystem(ROCK_REPLENISH_INTERVAL));
		addSystem(new MiningSystem(MINING_INTERVAL));

		addSystem(new FishReplenishSystem(FISH_REPLENISH_INTERVAL));
		addSystem(new FishingSystem(FISHING_INTERVAL));

		addSystem(new SpawningSystem(SPAWN_SYSTEM_INTERVAL));
		addSystem(new BattleSystem());
		addSystem(new RandomWalkSystem(RANDOM_MOVE_INTERVAL));
		addSystem(new LootSystem(LOOT_SYSTEM_INTERVAL));
		addSystem(new FollowSystem());
	}

	public static EntityManager getInstance() {
		return INSTANCE;
	}

	public static Entity getEntity(ImmutableArray<Entity> entities, Vector2Integer position) {
		for (Entity entity : entities) {
			if (Mappers.POS_CM.get(entity).position.equals(position)) {
				return entity;
			}
		}
		return null;
	}

	public static Entity getEntity(ImmutableArray<Entity> entities, int id) {
		for (Entity entity : entities) {
			if (Mappers.ID_CM.get(entity).id == id) {
				return entity;
			}
		}
		return null;
	}

	// FIXME Fix drop table storing
	public SlotComponent[] getNpcDropTable(int npcId) {
		ArrayList<SlotComponent> dropSlots = new ArrayList<SlotComponent>();
		for (int i = 0; i < World.DROP_BAG_DATA.size(); i++) {
			DropBagData dropBagData = World.DROP_BAG_DATA.get(i);
			if (dropBagData.NPC_ID == npcId) {
				dropSlots.add(new SlotComponent(World.getItem(dropBagData.ITEM_ID)));
			}
		}
		return dropSlots.toArray(new SlotComponent[dropSlots.size()]);
	}

	public static Entity getNPC(ImmutableArray<Entity> entities, int instanceId) {
		for (Entity entity : entities) {
			NPCComponent npcc = Mappers.NPC_CM.get(entity);
			if (npcc.instanceId == instanceId) {
				return entity;
			}
		}
		return null;
	}

	public Entity getPlayer(Connection connection) {
		for (Entity entity : getEntitiesFor(Family.all(IdComponent.class, ConnectionComponent.class).get())) {
			ConnectionComponent cc = Mappers.CON_CM.get(entity);
			if (cc.connection == connection) {
				return entity;
			}
		}
		return null;
	}

}