package com.realmofwrath.game.gameserver.network;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.utils.Array;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Server;
import com.realmofwrath.game.custom.Constants;
import com.realmofwrath.game.custom.Node;
import com.realmofwrath.game.custom.TiledUtils;
import com.realmofwrath.game.custom.Util;
import com.realmofwrath.game.custom.Vector2Integer;
import com.realmofwrath.game.gameserver.GameServer;
import com.realmofwrath.game.gameserver.custom.WorldMap;
import com.realmofwrath.game.gameserver.data.DropBagData;
import com.realmofwrath.game.gameserver.data.PlayerData;
import com.realmofwrath.game.gameserver.ecs.Mappers;
import com.realmofwrath.game.gameserver.ecs.components.FollowComponent;
import com.realmofwrath.game.gameserver.ecs.components.HealthComponent;
import com.realmofwrath.game.gameserver.ecs.components.MapComponent;
import com.realmofwrath.game.gameserver.ecs.components.MovementQueueComponent;
import com.realmofwrath.game.gameserver.ecs.components.NPCComponent;
import com.realmofwrath.game.gameserver.ecs.components.PositionComponent;
import com.realmofwrath.game.gameserver.ecs.components.SlotComponent;
import com.realmofwrath.game.gameserver.ecs.components.skills.SkillComponent;
import com.realmofwrath.game.gameserver.ecs.components.skills.fishing.FishingComponent;
import com.realmofwrath.game.gameserver.ecs.components.skills.mining.MiningComponent;
import com.realmofwrath.game.gameserver.ecs.components.skills.woodcutting.WoodcuttingComponent;
import com.realmofwrath.game.gameserver.ecs.entities.Item;
import com.realmofwrath.game.gameserver.ecs.entities.NPC;
import com.realmofwrath.game.gameserver.ecs.entities.Player;
import com.realmofwrath.game.gameserver.ecs.entities.data.NPCData;
import com.realmofwrath.game.gameserver.ecs.entities.environment.FishingSpot;
import com.realmofwrath.game.gameserver.ecs.entities.environment.Rock;
import com.realmofwrath.game.gameserver.ecs.entities.environment.Tree;
import com.realmofwrath.game.gameserver.ecs.systems.MovementSystem;
import com.realmofwrath.game.gameserver.ecs.systems.RandomWalkSystem;
import com.realmofwrath.game.gameserver.managers.EntityManager;
import com.realmofwrath.game.gameserver.sql.Database;
import com.realmofwrath.game.network.packet.PacketMove;
import com.realmofwrath.game.network.packet.PacketNPCKill;
import com.realmofwrath.game.network.packet.PacketRequestMapData;
import com.realmofwrath.game.network.packet.inventory.PacketInventoryData;
import com.realmofwrath.game.network.packet.inventory.PacketInventorySlotRemove;
import com.realmofwrath.game.network.packet.inventory.PacketInventorySlotUpdate;
import com.realmofwrath.game.network.packet.inventory.PacketRequestInventory;
import com.realmofwrath.game.network.packet.npc.PacketNPCSpawn;
import com.realmofwrath.game.network.packet.player.PacketClientData;
import com.realmofwrath.game.network.packet.player.PacketNewPlayer;
import com.realmofwrath.game.network.packet.player.PacketRemovePlayer;
import com.realmofwrath.game.network.packet.select.PacketSelectNPC;
import com.realmofwrath.game.network.packet.select.PacketSelectPlayer;
import com.realmofwrath.game.network.packet.select.PacketSelectSkillTile;
import com.realmofwrath.game.network.packet.select.PacketSelectTile;
import com.realmofwrath.game.network.packet.skill.fishing.PacketFishDepleted;
import com.realmofwrath.game.network.packet.skill.fishing.PacketFishReplenish;
import com.realmofwrath.game.network.packet.skill.mining.PacketRockMined;
import com.realmofwrath.game.network.packet.skill.mining.PacketRockReplenish;
import com.realmofwrath.game.network.packet.skill.woodcutting.PacketTreeCutDown;
import com.realmofwrath.game.network.packet.skill.woodcutting.PacketTreeRegrow;

public class World extends Listener {

	private final static World INSTANCE;

	private Server server;
	private EntityManager entityManager;
	private final List<WorldMap> maps;

	private final static Item[] ITEMS;
	private final static Map<Integer, NPCData> NPC_DATA;
	public final static ArrayList<DropBagData> DROP_BAG_DATA;

	static {
		ITEMS = Database.loadItems();
		NPC_DATA = Database.loadNPCData();
		DROP_BAG_DATA = Database.loadDropsData();
		INSTANCE = new World();
	}

	private World() {
		server = new Server();
		registerPackets();

		try {
			server.bind(GameServer.PORT, GameServer.PORT);
		} catch (IOException e) {
			e.printStackTrace();
		}

		server.addListener(this);
		server.start();

		entityManager = EntityManager.getInstance();

		// Load maps
		TmxMapLoader.Parameters parameters = new TmxMapLoader.Parameters();
		parameters.textureMinFilter = TextureFilter.Nearest;
		parameters.textureMagFilter = TextureFilter.Nearest;

		maps = new ArrayList<WorldMap>();

		int npcInstanceId = 0;

		for (int i = 0; i < 2; i++) {
			TiledMap tiledMap = new TmxMapLoader().load("maps/" + i + ".tmx", parameters);
			TiledMapTileLayer tileObjectLayer = (TiledMapTileLayer) tiledMap.getLayers().get(1);

			Array<Entity> rocks = new Array<Entity>(0);
			Array<Entity> trees = new Array<Entity>(0);
			Array<Entity> fishes = new Array<Entity>(0);

			// Load rocks & trees.
			for (int x = 0; x < Constants.TILE_SIZE; x++) {
				for (int y = 0; y < Constants.TILE_SIZE; y++) {
					TiledMapTile tile = TiledUtils.getTile(tileObjectLayer, x, y);
					if (tile != null) {
						Entity entity = null;

						// TODO HARDCODED min max values
						if (tile.getProperties().containsKey("resource")) {
							switch (tile.getProperties().get("name").toString()) {
							case "tree":
								entity = new Tree(i, TiledUtils.positionToMap(x, y), 1, 5);
								trees.add(entity);
								break;
							case "rock":
								entity = new Rock(i, TiledUtils.positionToMap(x, y), 1, 5);
								rocks.add(entity);
								break;
							case "fish":
								entity = new FishingSpot(i, TiledUtils.positionToMap(x, y), 1, 5);
								fishes.add(entity);
								break;
							default:
								continue;
							}

							entityManager.addEntity(entity);
						}
					}
				}
			}

			WorldMap worldMap = new WorldMap(i, tiledMap, new ImmutableArray<Entity>(rocks), new ImmutableArray<Entity>(trees), new ImmutableArray<Entity>(fishes));
			maps.add(worldMap);

			// Load npcs
			MapLayer npcLayer = tiledMap.getLayers().get("npcLayer");

			if (npcLayer != null) {
				for (MapObject mapObject : npcLayer.getObjects()) {
					int id = Integer.parseInt(mapObject.getProperties().get("id").toString());
					Vector2Integer position = new Vector2Integer(mapObject.getProperties().get("x", Float.class).intValue(), mapObject.getProperties().get("y", Float.class).intValue());
					NPCData npcData = NPC_DATA.get(id);
					List<Vector2Integer> area = RandomWalkSystem.getRandomWalkArea(worldMap, position.x, position.y, npcData.RADIUS);
					NPC entity = new NPC(npcInstanceId++, id, i, position.x, position.y, npcData.HEALTH, npcData.RESPAWN_TIME, npcData.ATTACK_TIME, npcData.TYPE, npcData.REACH, area);
					EntityManager.getInstance().addEntity(entity);
					worldMap.addNPC(entity);
				}
			}
		}

	}

	private void registerPackets() {
		Kryo kryo = server.getKryo();
		kryo.register(PacketInventorySlotUpdate.class);
		kryo.register(PacketInventorySlotRemove.class);
		kryo.register(PacketNPCSpawn.class);
		kryo.register(PacketClientData.class);
		kryo.register(PacketNewPlayer.class);
		kryo.register(PacketRemovePlayer.class);
		kryo.register(PacketSelectNPC.class);
		kryo.register(PacketSelectPlayer.class);
		kryo.register(PacketSelectSkillTile.class);
		kryo.register(PacketSelectTile.class);

		kryo.register(PacketRockMined.class);
		kryo.register(PacketRockReplenish.class);

		kryo.register(PacketTreeCutDown.class);
		kryo.register(PacketTreeRegrow.class);

		kryo.register(PacketMove.class);
		kryo.register(PacketNPCKill.class);
		kryo.register(PacketRequestMapData.class);

		kryo.register(PacketFishDepleted.class);
		kryo.register(PacketFishReplenish.class);

		kryo.register(PacketRequestInventory.class);
		kryo.register(PacketInventoryData.class);

		kryo.register(int[].class);
	}

	@Override
	public void connected(Connection connection) {
		PlayerData playerData = Database.loadPlayer(0);
		// Send client ID to the player
		PacketClientData packetClientData = new PacketClientData();
		packetClientData.id = connection.getID();
		packetClientData.name = playerData.getName();
		packetClientData.mapId = playerData.getMapId();
		packetClientData.x = playerData.getPosition().x;
		packetClientData.y = playerData.getPosition().y;
		connection.sendTCP(packetClientData);

		// Create player
		// TODO Auth & get id. 
		Entity player = new Player(connection, playerData, Database.getPlayerInventory(0));

		WorldMap worldMap = maps.get(Mappers.MAP_CM.get(player).mapId);

		MovementQueueComponent mqc = Mappers.MQU_CM.get(player);
		Vector2Integer position = (mqc.currentMove == null) ? TiledUtils.positionToTiled(Mappers.POS_CM.get(player).position) : mqc.currentMove;
		PacketNewPlayer packetNewPlayer = new PacketNewPlayer();
		packetNewPlayer.id = Mappers.ID_CM.get(player).id;
		packetNewPlayer.x = position.x;
		packetNewPlayer.y = position.y;

		worldMap.sendToAll(packetNewPlayer);

		worldMap.addPlayer(player);

		entityManager.addEntity(player);
	}

	@Override
	public void disconnected(Connection connection) {
		Entity player = entityManager.getPlayer(connection);

		maps.get(Mappers.MAP_CM.get(player).mapId).removePlayer(player);
		entityManager.removeEntity(player);
		// FIXME att removal
		//		// Remove attacker victims
		//		AttackComponent ac = Mappers.ATT_CM.get(player);
		//
		//		ac.attackers.add(0, ac.victim);
		//
		//		for (Entity entity : ac.attackers) {
		//			AttackComponent entityAttackComponent = Mappers.ATT_CM.get(entity);
		//			if (entityAttackComponent.victim == player) {
		//				entityAttackComponent.victim = null;
		//				Mappers.STA_CM.get(entity).state = EntityState.IDLE;
		//			}
		//
		//			entityAttackComponent.attackers.remove(player);
		//		}

	}

	@Override
	public void received(Connection connection, Object object) {
		Entity player = entityManager.getPlayer(connection);

		if (player != null) {
			if (object instanceof PacketRequestMapData) {
				WorldMap worldMap = maps.get(Mappers.MAP_CM.get(player).mapId);

				// Sync players
				for (Entity entity : worldMap.getPlayers()) {
					if (entity != player) {
						MovementQueueComponent mqc = Mappers.MQU_CM.get(entity);
						Vector2Integer position = (mqc.currentMove == null) ? TiledUtils.positionToTiled(Mappers.POS_CM.get(entity).position) : mqc.currentMove;
						PacketNewPlayer packet = new PacketNewPlayer();
						packet.id = Mappers.ID_CM.get(entity).id;
						packet.x = position.x;
						packet.y = position.y;
						connection.sendTCP(packet);
					}
				}

				// Sync rocks that are mined
				for (Entity entity : worldMap.getRocks()) {
					if (Mappers.REP_CM.get(entity) != null) {
						Vector2Integer position = TiledUtils.positionToTiled(Mappers.POS_CM.get(entity).position);
						PacketRockMined packet = new PacketRockMined();
						packet.x = position.x;
						packet.y = position.y;
						connection.sendTCP(packet);
					}
				}

				// Sync trees that are cut down
				for (Entity entity : worldMap.getTrees()) {
					if (Mappers.REP_CM.get(entity) != null) {
						Vector2Integer position = TiledUtils.positionToTiled(Mappers.POS_CM.get(entity).position);
						PacketTreeCutDown packet = new PacketTreeCutDown();
						packet.x = position.x;
						packet.y = position.y;
						connection.sendTCP(packet);
					}
				}

				// Sync fishing spots that are depleted
				for (Entity entity : worldMap.getFishes()) {
					if (Mappers.REP_CM.get(entity) != null) {
						Vector2Integer position = TiledUtils.positionToTiled(Mappers.POS_CM.get(entity).position);
						PacketFishDepleted packet = new PacketFishDepleted();
						packet.x = position.x;
						packet.y = position.y;
						connection.sendTCP(packet);
					}
				}

				// Send NPC's.
				for (Entity entity : worldMap.getNPCs()) {
					HealthComponent hc = Mappers.HP_CM.get(entity);
					if (hc.health > 0) {
						NPCComponent nc = Mappers.NPC_CM.get(entity);
						PositionComponent pc = Mappers.POS_CM.get(entity);
						MovementQueueComponent mqc = Mappers.MQU_CM.get(entity);
						Vector2Integer position = (mqc.currentMove != null) ? mqc.currentMove : TiledUtils.positionToTiled(pc.position);
						PacketNPCSpawn packet = new PacketNPCSpawn();
						packet.id = nc.id;
						packet.instanceId = nc.instanceId;
						packet.x = position.x;
						packet.y = position.y;
						packet.health = hc.health;
						packet.maxHealth = hc.maxHealth;
						connection.sendTCP(packet);
					}
				}

				// TODO Send Drop bags.

			} else if (object instanceof PacketSelectSkillTile) {
				PacketSelectSkillTile packet = (PacketSelectSkillTile) object;

				if (Mappers.HP_CM.get(player).health > 0) {
					Vector2Integer resourcePosition = new Vector2Integer(packet.x, packet.y);
					Vector2Integer positionBesideResource = new Vector2Integer(packet.tX, packet.tY);

					MapComponent mc = Mappers.MAP_CM.get(player);
					WorldMap worldMap = maps.get(mc.mapId);
					TiledMapTile tile = TiledUtils.getTile(worldMap.getTileObjectLayer(), resourcePosition);

					// Deal with map objects (trees, etc)
					if (tile != null) {
						SkillComponent skillComponent = null;
						ImmutableArray<Entity> entities = null;

						switch (tile.getProperties().get("name").toString()) {
						case "tree":
							skillComponent = new WoodcuttingComponent();
							entities = worldMap.getTrees();
							break;
						case "rock":
							skillComponent = new MiningComponent();
							entities = worldMap.getRocks();
							break;
						case "fish":
							skillComponent = new FishingComponent();
							entities = worldMap.getFishes();
							break;
						default:
							System.err.println("UNKNOWN RESOURCE - World.java");
							return;
						}

						MovementQueueComponent mqc = Mappers.MQU_CM.get(player);
						Vector2Integer currentMove = mqc.currentMove;

						Vector2Integer startPosition = (currentMove != null) ? currentMove : TiledUtils.positionToTiled(Mappers.POS_CM.get(player).position);
						if (Vector2Integer.euclideanDistance(startPosition, resourcePosition) > 1f) {
							List<Node> path = Util.findPath(worldMap, startPosition, positionBesideResource, true, false);
							if (!path.isEmpty()) {
								reset(player);
								MovementSystem.overwritePath(mqc, path);
								skillComponent.interactee = EntityManager.getEntity(entities, TiledUtils.positionToMap(resourcePosition));
								player.add(skillComponent);
							} else {
								System.err.println("PATH NOT FOUND");
							}
						} else { // Player is standing beside a resource entity.
							SkillComponent rsc = Mappers.SKI_CM.get(player);
							Entity resourceEntity = EntityManager.getEntity(entities, TiledUtils.positionToMap(resourcePosition));
							if (rsc == null || rsc.interactee != player) {
								reset(player);
								skillComponent.interactee = resourceEntity;
								player.add(skillComponent);
							}
						}
					}
				} else {
					System.err.println("PLAYER IS DEAD");
				}
			} else if (object instanceof PacketSelectTile) {
				PacketSelectTile packet = (PacketSelectTile) object;

				if (Mappers.HP_CM.get(player).health > 0) {
					MovementQueueComponent mqc = Mappers.MQU_CM.get(player);
					Vector2Integer start = (mqc.currentMove != null) ? mqc.currentMove : TiledUtils.positionToTiled(Mappers.POS_CM.get(player).position);

					Vector2Integer destination = new Vector2Integer(packet.x, packet.y);

					if (!destination.equals(start)) {
						List<Node> path = Util.findPath(maps.get(Mappers.MAP_CM.get(player).mapId), start, destination, true, false);
						reset(player);
						if (path.size() > 0) {
							MovementSystem.overwritePath(mqc, path);
						} else {
							System.err.println("PATH NOT FOUND!");
						}
					} else {
						System.err.println("PLAYER IS ALREADY ON THIS TILE!");
					}
				} else {
					System.err.println("SINCE PLAYER IS DEAD, IT CAN NOT MOVE!");
				}
			} else if (object instanceof PacketSelectNPC) {
				PacketSelectNPC packet = (PacketSelectNPC) object;
				WorldMap worldMap = maps.get(Mappers.MAP_CM.get(player).mapId);
				Entity entity = EntityManager.getNPC(worldMap.getNPCs(), packet.instanceId);
				if (Mappers.HP_CM.get(player).health > 0) {
					if (entity != null) {
						if (Mappers.HP_CM.get(entity).health > 0) {
							FollowComponent fc = Mappers.FOL_CM.get(player);
							if (fc == null || fc.subject != entity) {
								Mappers.MQU_CM.get(player).queue.clear();
								player.add(new FollowComponent(entity));
							}
						}
					}
				}
			} else if (object instanceof PacketSelectPlayer) {
				PacketSelectPlayer packet = (PacketSelectPlayer) object;
				if (Mappers.HP_CM.get(player).health > 0) {
					WorldMap worldMap = maps.get(Mappers.MAP_CM.get(player).mapId);
					System.out.println(packet.id);
					Entity playerToFollow = EntityManager.getEntity(worldMap.getPlayers(), packet.id);
					if (playerToFollow != null && playerToFollow != player) {
						if (Mappers.HP_CM.get(playerToFollow).health > 0) {
							FollowComponent fc = Mappers.FOL_CM.get(player);
							if (fc == null || fc.subject != playerToFollow) {
								Mappers.MQU_CM.get(player).queue.clear();
								player.add(new FollowComponent(playerToFollow));
								System.out.println("START FOLLOWING");
							}
						}
					}
				}
			} else if (object instanceof PacketRequestInventory) {
				PacketInventoryData packet = new PacketInventoryData();

				SlotComponent[] slots = Mappers.INV_CM.get(player).slots;
				packet.items = new int[slots.length];

				for (int i = 0; i < slots.length; i++) {
					int itemId = -1;
					if (slots[i].item != null) {
						itemId = Mappers.ID_CM.get(slots[i].item).id;
					}
					packet.items[i] = itemId;
				}

				connection.sendTCP(packet);
			}
		}
	}

	public static Item getItem(int id) {
		return ITEMS[id];
	}

	public List<WorldMap> getMaps() {
		return maps;
	}

	public static World getInstance() {
		return INSTANCE;
	}

	public static void reset(Entity entity) {
		entity.remove(WoodcuttingComponent.class);
		entity.remove(FollowComponent.class);
		entity.remove(MiningComponent.class);
		entity.remove(FishingComponent.class);
		Mappers.ATT_CM.get(entity).victim = null;
	}

}