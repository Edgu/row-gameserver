package com.realmofwrath.game.gameserver.sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import com.realmofwrath.game.gameserver.Logger;
import com.realmofwrath.game.gameserver.data.DropBagData;
import com.realmofwrath.game.gameserver.data.PlayerData;
import com.realmofwrath.game.gameserver.ecs.components.NameComponent;
import com.realmofwrath.game.gameserver.ecs.components.SlotComponent;
import com.realmofwrath.game.gameserver.ecs.components.items.AttributeComponent;
import com.realmofwrath.game.gameserver.ecs.entities.Item;
import com.realmofwrath.game.gameserver.ecs.entities.data.NPCData;
import com.realmofwrath.game.gameserver.ecs.systems.InventorySystem;
import com.realmofwrath.game.gameserver.network.World;
import com.realmofwrath.game.inventory.Attribute;
import com.realmofwrath.game.inventory.ItemType;

public abstract class Database {

	// Database credentials
	private static String database = "realmofwrath";
	private static String userName = "realmofwrath";
	private static String password = "LF98L6mTncuwFNJh";

	// JDBC driver name and database URL
	private static String driver = "com.mysql.jdbc.Driver";
	private static String url = "jdbc:mysql://server.edgu.lt:3306/" + database + "?autoReconnect=true";
	private final static String KEEP_ALIVE = "/* ping */ SELECT 1";

	private static Connection connection;
	private static volatile Statement statement;

	public static void init(Logger logger) {
		try {
			// STEP 2: Register JDBC driver
			Class.forName(driver);

			// STEP 3: Open a connection
			logger.output("[MySQL] <Connecting to database...>");
			connection = DriverManager.getConnection(url, userName, password);

			// STEP 4: Execute a query
			statement = connection.createStatement();
			statement.executeQuery(KEEP_ALIVE);
			logger.output("[MySQL] <Succesfully connected to the database>");
		} catch (Exception exception) {
			exception.printStackTrace();
		}

		Timer timer = new Timer();
		timer.schedule(new TimerTask() {
			public void run() {
				try {
					statement.executeQuery(KEEP_ALIVE);
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}, 10 * 60 * 1000, 10 * 60 * 1000);
	}

	public static Item[] loadItems() {
		try {
			Statement itemsStatement = connection.createStatement();

			ResultSet itemRS = itemsStatement.executeQuery("SELECT id, type, name FROM item");

			if (itemRS.last()) {
				Item[] items = new Item[itemRS.getRow()];
				itemRS.first();

				int i = 0;
				Statement attributesStatement = connection.createStatement();

				do {
					ResultSet itemAttributeRS = attributesStatement.executeQuery("SELECT attribute_id, value FROM item_attribute WHERE item_id = " + i);

					if (itemAttributeRS.last()) {
						AttributeComponent[] attributes = new AttributeComponent[itemAttributeRS.getRow()];
						itemAttributeRS.first();

						int j = 0;

						// Create attributes for current item
						do {
							attributes[j] = new AttributeComponent(Attribute.getAttribute(itemAttributeRS.getInt("attribute_id")), itemAttributeRS.getInt("value"));
							j++;
						} while (itemAttributeRS.next());
						items[i] = new Item(itemRS.getInt("id"), itemRS.getString("name"), ItemType.getType(itemRS.getInt("type")), attributes);
						System.out.println("[loadItems]: itemId = " + i + ", name = " + items[i].getComponent(NameComponent.class).name);
					} else {
						items[i] = new Item(itemRS.getInt("id"), itemRS.getString("name"), ItemType.getType(itemRS.getInt("type")), null);
						System.out.println("[loadItems]: itemId = " + i + ", name = " + items[i].getComponent(NameComponent.class).name + ", WARNING = Item has no attributes!");
					}
					i++;
				} while(itemRS.next());

				attributesStatement.close();
				itemsStatement.close();

				return items;
			}
			itemsStatement.close();
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return null;
	}

	public static boolean giveItem(int playerId, int slotId, int itemId) {
		try {
			NamedParameterStatement nps = new NamedParameterStatement(connection, "INSERT INTO inventory (player_id, slot_id, item_id) VALUES (:player_id, :slot_id, :item_id)");
			nps.setString("player_id", Integer.toString(playerId));
			nps.setString("slot_id", Integer.toString(slotId));
			nps.setString("item_id", Integer.toString(itemId));
			int rows = nps.executeUpdate();
			nps.close();
			return rows == 1;
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return false;
	}

	public static boolean removeItem(int playerId, int slotId) {
		try {
			NamedParameterStatement nps = new NamedParameterStatement(connection, "DELETE FROM inventory WHERE player_id = :player_id AND slot_id = :slot_id");
			nps.setString("player_id", Integer.toString(playerId));
			nps.setString("slot_id", Integer.toString(slotId));
			int rows = nps.executeUpdate();
			nps.close();
			return rows == 1;
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return false;
	}

	public static SlotComponent[] getPlayerInventory(int id) {
		try {
			NamedParameterStatement nps = new NamedParameterStatement(connection, "SELECT * FROM inventory WHERE player_id = :player_id");
			nps.setInt("player_id", id);
			ResultSet rs = nps.executeQuery();

			SlotComponent[] sc = InventorySystem.createPlayerInventorySlots();

			if (rs.first()) {
				do {
					sc[rs.getInt("slot_id")].item = World.getItem(rs.getInt("item_id"));
				} while (rs.next());
			}

			nps.close();
			return sc;
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return null;
	}

	public static PlayerData loadPlayer(int id) {
		PlayerData playerData = null;
		try {
			NamedParameterStatement nps = new NamedParameterStatement(connection, "SELECT id, name, map_id, x, y FROM player WHERE id = :id LIMIT 1");
			nps.setInt("id", id);
			ResultSet rs = nps.executeQuery();

			if (rs.first()) {
				playerData = new PlayerData(rs.getInt("id"), rs.getString("name"), rs.getInt("map_id"), rs.getInt("x"), rs.getInt("y"));
			}
			nps.close();
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return playerData;
	}

	public static ArrayList<DropBagData> loadDropsData() {
		ArrayList<DropBagData> dropBagData = new ArrayList<DropBagData>();
		try {
			Statement statement = connection.createStatement();
			ResultSet rs = statement.executeQuery("SELECT * FROM loot");

			if (rs.first()) {
				do {
					// TODO Store npcs drops in arrays in drop data? Perhaps hashmap?
					dropBagData.add(new DropBagData(rs.getInt("npc_id"), rs.getInt("item_id"), rs.getInt("rarity_id"), rs.getInt("chance")));
				} while (rs.next());
			}

			statement.close();
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return dropBagData;
	}

	public static Map<Integer, NPCData> loadNPCData() {
		Map<Integer, NPCData> npcData = new HashMap<Integer, NPCData>();
		try {
			Statement statement = connection.createStatement();
			ResultSet rs = statement.executeQuery("SELECT * FROM npc");

			if (rs.first()) {
				do {
					npcData.put(rs.getInt("id"), new NPCData(rs.getInt("type"), rs.getString("name"), rs.getInt("health"), rs.getInt("damage"), rs.getInt("respawn_time"), rs.getInt("attack_time"), rs.getInt("reach"), rs.getInt("loot_id"), rs.getInt("radius")));
				} while (rs.next());
			}

			statement.close();
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return npcData;
	}

	public static void terminate() throws SQLException {
		statement.executeUpdate("UPDATE players SET tempID = 0, online = 0");
	}

}